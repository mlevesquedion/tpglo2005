# Instructions

## Démarrage
1. Dans le fichier "db_password.txt", écrire votre mot de passe de BD
2. Exécuter le script "setup_database.py" (ceci peut prendre quelques minutes)
3. Partir le serveur d'application (main.py)
4. Ouvrir le site à l'adresse localhost:5000
5. Vous pouvez vous faire un compte ou vous connecter avec les informations
dans le fichier "admin_account.txt". Ce compte n'a pas de capacités spéciales
et sert seulement à permettre de voir les fonctionnalités du site.

## Exécuter les tests
Il y a deux batteries de tests principales : une pour la base de données
et une pour le site web. Il y a aussi une batterie de tests pour les fonctions
qui génèrent des fausses données. Les tests ont été écrits avec selenium et pytest.
Pour exécuter les tests, ouvrir un terminal dans la racine du projet et exécuter
les commandes suivantes :

* pytest data_mocking
* pytest functional_tests
* pytest sql/tests

Les tests peuvent prendre un certain temps à rouler, surtout les tests
avec selenium (functional_tests). Il arrive aussi que selenium "bloque"
pendant l'exécution des tests, auquel cas il faut redémarrer le test.

## Gitlab

Voici l'adresse de notre projet sur Gitlab : https://gitlab.com/mlevesquedion/tpglo2005