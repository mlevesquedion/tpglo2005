def test_access_edit(browser_logged_in):
    browser = browser_logged_in

    edit_link = browser.find_element_by_id('edit')
    edit_link.click()

    assert 'Modification des informations' in browser.page_source

    email_field = browser.find_element_by_name('email')
    assert email_field.get_attribute('value') == 'admin@gmail.com'


def test_update_invalid(browser_logged_in):
    browser = browser_logged_in

    browser.get('http://127.0.0.1:5000/user_edit/1')

    email_field = browser.find_element_by_name('email')
    email_field.send_keys('meowmeow')

    submit_button = browser.find_element_by_id('submit')
    submit_button.click()

    assert 'Email invalide' in browser.page_source


def test_update_valid(browser_logged_in, data_updater):
    browser = browser_logged_in

    browser.get('http://127.0.0.1:5000/user_edit/1')

    email_field = browser.find_element_by_name('email')
    email_field.clear()
    email_field.send_keys('meowmeow@catmail.com')

    submit_button = browser.find_element_by_id('submit')
    submit_button.click()

    assert 'succès' in browser.page_source

    email_field = browser.find_element_by_name('email')
    assert email_field.get_attribute('value') == 'meowmeow@catmail.com'

    data_updater.update_user(1, {
        'email': 'admin@gmail.com'
    })
