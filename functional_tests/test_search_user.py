def test_search_user(browser_logged_in):
    browser = browser_logged_in

    search_input = browser.find_element_by_name('search_txt')
    search_input.send_keys('Admin')

    search_button = browser.find_element_by_name('submit')
    search_button.click()

    assert 'Recherche pour Admin' in browser.page_source
    assert browser.page_source.count('Admin') >= 2
