def test_valid_login(browser):
    email = browser.find_element_by_name('email')
    email.send_keys('admin@gmail.com')
    password = browser.find_element_by_name('password')
    password.send_keys('admin')

    submit = browser.find_element_by_id('submit')
    submit.click()

    assert 'Admin' in browser.page_source


def test_invalid_login(browser):
    email = browser.find_element_by_name('email')
    email.send_keys('admin@gmail.com')
    password = browser.find_element_by_name('password')
    password.send_keys('admins')

    submit = browser.find_element_by_id('submit')
    submit.click()

    assert 'Connexion impossible' in browser.page_source
