def test_click_hashtag(browser_logged_in):
    browser = browser_logged_in

    browser.get('http://127.0.0.1:5000/publications/')

    hashtag = browser.find_element_by_class_name('hashtag-link')
    hashtag_name = hashtag.text
    hashtag.click()

    assert browser.page_source.count(hashtag_name) >= 10
