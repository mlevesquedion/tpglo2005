def test_cancel_delete(browser, data_updater):
    browser.get('http://127.0.0.1:5000/register')

    text_fields = {
        'email': "meowmeow@catmail.com",
        'password': 'password',
        'name': 'Meowmeow',
        'telephone': '418 123-4567',
        'date': '2017\t0913'
    }

    for field_name, value in text_fields.items():
        browser.find_element_by_name(field_name).send_keys(value)

    submit_button = browser.find_element_by_id('submit')
    submit_button.click()

    email = browser.find_element_by_name('email')
    email.send_keys('meowmeow@catmail.com')
    password = browser.find_element_by_name('password')
    password.send_keys('password')

    submit = browser.find_element_by_id('submit')
    submit.click()

    edit_link = browser.find_element_by_id('edit')
    edit_link.click()

    delete_button = browser.find_element_by_id('delete-account-button')
    delete_button.click()

    alert = browser.switch_to.alert
    assert 'Êtes vous sûr de vouloir supprimer votre compte?' in alert.text
    alert.dismiss()

    alert = browser.switch_to.alert
    assert "Votre compte n'a pas été supprimé" in alert.text
    alert.accept()

    data_updater.delete_account_by_email('meowmeow@catmail.com')


def test_delete_account(browser, data_updater):
    browser.get('http://127.0.0.1:5000/register')

    text_fields = {
        'email': "meowmeow@catmail.com",
        'password': 'password',
        'name': 'Meowmeow',
        'telephone': '418 123-4567',
        'date': '2017\t0913'
    }

    for field_name, value in text_fields.items():
        browser.find_element_by_name(field_name).send_keys(value)

    submit_button = browser.find_element_by_id('submit')
    submit_button.click()

    email = browser.find_element_by_name('email')
    email.send_keys('meowmeow@catmail.com')
    password = browser.find_element_by_name('password')
    password.send_keys('password')

    submit = browser.find_element_by_id('submit')
    submit.click()

    edit_link = browser.find_element_by_id('edit')
    edit_link.click()

    delete_button = browser.find_element_by_id('delete-account-button')
    delete_button.click()

    alert = browser.switch_to.alert
    assert 'Êtes vous sûr de vouloir supprimer votre compte?' in alert.text
    alert.accept()

    assert 'Votre compte a été supprimé' in browser.page_source

    email = browser.find_element_by_name('email')
    email.send_keys('meowmeow@catmail.com')
    password = browser.find_element_by_name('password')
    password.send_keys('password')

    submit = browser.find_element_by_id('submit')
    submit.click()

    assert "Connexion impossible. Nom d'utilisateur ou mot de passe invalide" in browser.page_source
