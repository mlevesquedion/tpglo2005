from random import choice


def test_unfriend(browser_logged_in, data_fetcher, data_inserter, data_updater):
    browser = browser_logged_in

    admin_friend_ids = data_fetcher.get_user_friend_ids(1)

    friend_id = choice(admin_friend_ids)

    browser.get('http://127.0.0.1:5000/user_page/{}'.format(friend_id))

    assert 'Mettre fin à cette belle amitié' in browser.page_source

    unfriend_button = browser.find_element_by_id('friend-button')
    unfriend_button.click()

    assert "Vous n'êtes pas ami avec" in browser.page_source

    assert friend_id not in data_fetcher.get_user_friend_ids(1)

    data_inserter.insert_demandeAmitie([1, friend_id])
    data_updater.accept_friend_request(1, friend_id)


def test_send_friend_request(browser_logged_in, data_fetcher, data_updater):
    browser = browser_logged_in

    admin_friend_ids = set(data_fetcher.get_user_friend_ids(1))

    admin_friend_requested_ids = set([tuple['id_cible'] for tuple in data_fetcher.run_parameterized_query("""\
    SELECT id_cible
    FROM demandeAmitie
    WHERE id_source=1 AND etat=FALSE""")])

    not_friend_ids = set(range(2, 100)) - (admin_friend_ids | admin_friend_requested_ids)
    not_friend_id = choice(list(not_friend_ids))

    browser.get('http://127.0.0.1:5000/user_page/{}'.format(not_friend_id))

    assert "Vous n'êtes pas ami avec" in browser.page_source

    send_request_button = browser.find_element_by_id('friend-button')
    send_request_button.click()

    assert data_fetcher.run_parameterized_query('''
    SELECT TRUE
    FROM demandeAmitie
    WHERE id_source=1 AND id_cible={}'''.format(not_friend_id)) is not None

    assert "Vous n'êtes pas ami avec" in browser.page_source

    assert "Demande d'amitié envoyée" in browser.page_source

    data_updater.run_parameterized_query('''
    DELETE FROM demandeAmitie
    WHERE id_source=1 AND id_cible={}'''.format(not_friend_id))


def test_accept_friend_request(browser_logged_in, data_fetcher, data_updater, data_inserter):
    browser = browser_logged_in

    requester_id = data_fetcher.run_parameterized_query('''
    SELECT id_source
    FROM demandeAmitie
    WHERE id_cible=1 AND etat=FALSE''')

    if requester_id:
        requester_id = requester_id[0]['id_source']
    else:
        admin_friend_ids = set(data_fetcher.get_user_friend_ids(1))

        admin_friend_requested_ids = set([tuple['id_cible'] for tuple in data_fetcher.run_parameterized_query("""\
            SELECT id_cible
            FROM demandeAmitie
            WHERE id_source=1 AND etat=FALSE""")])

        not_friend_ids = set(range(2, 100)) - (admin_friend_ids | admin_friend_requested_ids)
        requester_id = choice(list(not_friend_ids))

        print(not_friend_ids)

        print(requester_id)
        data_updater.unfriend(requester_id, 1)
        data_inserter.insert_demandeAmitie([requester_id, 1])

    browser.get('http://127.0.0.1:5000/user_page/{}'.format(requester_id))

    assert "Vous n'êtes pas ami avec" in browser.page_source

    accept_request_button = browser.find_element_by_id('friend-button')
    accept_request_button.click()

    assert requester_id in data_fetcher.get_user_friend_ids(1)

    assert "Vous êtes ami avec" in browser.page_source

    data_updater.unfriend(requester_id, 1)

    data_inserter.insert_demandeAmitie([requester_id, 1])
