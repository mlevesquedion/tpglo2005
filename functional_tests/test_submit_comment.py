from time import sleep


def test_submit_comment_invalid(browser_logged_in):
    browser = browser_logged_in

    browser.get('http://127.0.0.1:5000/publications/')

    comment_button = browser.find_elements_by_class_name('comment-button')[0]
    comment_button.click()

    alert = browser.switch_to.alert
    alert.accept()


def test_submit_comment_valid(browser_logged_in, data_updater):
    browser = browser_logged_in

    browser.get('http://127.0.0.1:5000/publications/')

    comment_textarea = browser.find_elements_by_class_name('comment-textarea')[0]
    comment_textarea.send_keys('Meow meow meow')

    comment_button = browser.find_elements_by_class_name('comment-button')[0]
    comment_button.click()

    sleep(1)

    assert 'Meow meow meow' in browser.page_source

    data_updater.run_parameterized_query("""\
    DELETE FROM publicationCommentaire
    WHERE contenu = 'Meow meow meow'""")
