from time import sleep


def test_submit_publication_valid(browser_logged_in, data_fetcher):
    browser = browser_logged_in

    browser.get('http://127.0.0.1:5000/publications/')

    text_area = browser.find_element_by_id('contenu')
    text_area.send_keys('Meow meow meow meow')

    submit_button = browser.find_element_by_id('submit')
    submit_button.click()

    sleep(1)

    assert 'Meow meow meow meow' in browser.page_source

    delete_button = browser.find_elements_by_class_name('delete-publication-button')[0]
    delete_button.click()

    sleep(2)

    assert 'Meow meow meow meow' not in browser.page_source

    assert not data_fetcher.run_parameterized_query("""\
    SELECT * FROM publication
    WHERE contenu='Meow meow meow meow'""")