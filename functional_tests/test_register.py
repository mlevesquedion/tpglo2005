from time import sleep


def test_error_form_not_filled_in(browser):
    browser.get('http://127.0.0.1:5000/register')

    submit_button = browser.find_element_by_id('submit')
    submit_button.click()

    source = browser.page_source
    assert 'Création de compte' in source
    assert 'Email invalide' in source


def test_error_user_exists(browser):
    browser.get('http://127.0.0.1:5000/register')

    text_fields = {
        'email': "admin@gmail.com",
        'password': 'password',
        'name': 'Meowmeow',
        'telephone': '418 123-4567',
        'date': '2017\t0913'
    }

    for field_name, value in text_fields.items():
        browser.find_element_by_name(field_name).send_keys(value)

    submit_button = browser.find_element_by_id('submit')
    submit_button.click()

    source = browser.page_source
    assert 'Vous avez déjà un compte' in source


def test_valid(browser, data_fetcher, data_updater):
    browser.get('http://127.0.0.1:5000/register')

    text_fields = {
        'email': "meowmeow@catmail.com",
        'password': 'password',
        'name': 'Meowmeow',
        'telephone': '418 123-4567',
        'date': '2017\t0913'
    }

    for field_name, value in text_fields.items():
        browser.find_element_by_name(field_name).send_keys(value)

    submit_button = browser.find_element_by_id('submit')
    submit_button.click()

    sleep(1)

    source = browser.page_source
    assert 'Votre compte a été créé avec succès' in source

    assert data_fetcher.get_user_data_by_email('meowmeow@catmail.com')

    email_field = browser.find_element_by_name('email')
    email_field.send_keys('admin@gmail.com')

    submit_button = browser.find_element_by_id('submit')
    submit_button.click()

    data_updater.delete_account_by_email('meowmeow@catmail.com')
