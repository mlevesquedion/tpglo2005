def test_logout(browser_logged_in):
    browser = browser_logged_in

    logout_button = browser.find_element_by_id('logout')
    logout_button.click()

    assert 'Vous vous êtes déconnecté' in browser.page_source
