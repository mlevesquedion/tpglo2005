from random import choice


def test_access_own_page(browser_logged_in):
    browser = browser_logged_in

    user_page_link = browser.find_element_by_id('user-page')
    user_page_link.click()

    assert 'Bienvenue sur votre page personnelle' in browser.page_source


def test_access_friend_page(browser_logged_in, data_fetcher):
    browser = browser_logged_in

    admin_friend_ids = data_fetcher.get_user_friend_ids(1)

    friend_id = choice(admin_friend_ids)

    browser.get('http://127.0.0.1:5000/user_page/{}'.format(friend_id))

    assert 'Vous êtes ami avec' in browser.page_source


def test_access_not_friend_page(browser_logged_in, data_fetcher):
    browser = browser_logged_in

    admin_friend_ids = set(data_fetcher.get_user_friend_ids(1))

    not_friend_ids = set(range(2, 100)) - admin_friend_ids

    not_friend_id = choice(list(not_friend_ids))

    browser.get('http://127.0.0.1:5000/user_page/{}'.format(not_friend_id))

    assert "Vous n'êtes pas ami avec" in browser.page_source
