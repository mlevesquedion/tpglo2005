from pytest import fixture
from selenium.webdriver import Chrome
from sql.DataFetcher import DataFetcher
from sql.DataInserter import DataInserter
from sql.DataUpdater import DataUpdater


@fixture()
def browser():

    browser = Chrome()

    browser.get('http://127.0.0.1:5000/')

    yield browser

    browser.quit()


@fixture()
def browser_logged_in():

    browser = Chrome()

    browser.get('http://127.0.0.1:5000/')

    email = browser.find_element_by_name('email')
    email.send_keys('admin@gmail.com')
    password = browser.find_element_by_name('password')
    password.send_keys('admin')

    submit = browser.find_element_by_id('submit')
    submit.click()

    yield browser

    browser.quit()


@fixture()
def data_fetcher():

    data_fetcher = DataFetcher()

    yield data_fetcher

    data_fetcher.close()


@fixture()
def data_updater():

    data_updater = DataUpdater()

    yield data_updater

    data_updater.close()


@fixture()
def data_inserter():

    data_inserter = DataInserter()

    yield data_inserter

    data_inserter.close()