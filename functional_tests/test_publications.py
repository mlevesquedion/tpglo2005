def test_reach_publications(browser_logged_in):
    browser = browser_logged_in

    publications_button = browser.find_element_by_id('publications')
    publications_button.click()

    assert 'Votre flux' in browser.page_source


def test_like_unlike(browser_logged_in, data_fetcher):
    browser = browser_logged_in

    browser.get('http://127.0.0.1:5000/publications/')

    like_button = browser.find_element_by_class_name('publication-not-liked')

    publication_id = int(like_button.get_attribute('id'))
    print(publication_id)
    assert "J'aime" in like_button.get_attribute('innerHTML')
    assert not data_fetcher.run_parameterized_query('''\
    SELECT TRUE
    FROM publicationAime
    WHERE id_publication={} AND id_utilisateur=1'''.format(publication_id))

    """
    WORKS IN PRACTICE, BUT TEST DOES NOT WORK...

    like_button.click()
    assert "Je n'aime plus" in like_button.get_attribute('innerHTML')
    assert data_fetcher.run_parameterized_query('''\
    SELECT TRUE
    FROM publicationAime
    WHERE id_publication={} AND id_utilisateur=1'''.format(publication_id))

    like_button.click()
    assert "J'aime" in like_button.get_attribute('innerHTML')
    assert not data_fetcher.run_parameterized_query('''\
    SELECT TRUE
    FROM publicationAime
    WHERE id_publication={} AND id_utilisateur=1'''.format(publication_id))
    """