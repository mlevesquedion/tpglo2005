def test_submit_comment_invalid(browser_logged_in):
    browser = browser_logged_in

    browser.get('http://127.0.0.1:5000/publications/')

    assert 'Nouvelle publication' in browser.page_source

    submit_button = browser.find_element_by_id('submit')
    submit_button.click()

    assert 'Votre publication ne peut pas être vide' in browser.page_source


def test_submit_publication_valid(browser_logged_in, data_updater):
    browser = browser_logged_in

    browser.get('http://127.0.0.1:5000/publications/')

    assert 'Nouvelle publication' in browser.page_source

    text_area = browser.find_element_by_id('contenu')
    text_area.send_keys('Meow meow meow')

    submit_button = browser.find_element_by_id('submit')
    submit_button.click()

    assert 'Meow meow meow' in browser.page_source

    data_updater.run_parameterized_query("""\
    DELETE FROM publication
    WHERE contenu='Meow meow meow'""")


def test_submit_publication_invalid_hashtag(browser_logged_in):
    browser = browser_logged_in

    browser.get('http://127.0.0.1:5000/publications/')

    assert 'Nouvelle publication' in browser.page_source

    text_area = browser.find_element_by_id('contenu')
    text_area.send_keys('Meow meow meow')

    hashtag_area = browser.find_element_by_id('hashtags')
    hashtag_area.send_keys('Meow meow meow')

    submit_button = browser.find_element_by_id('submit')
    submit_button.click()

    assert 'Les hashtags doivent débuter par un # et être séparés par des virgules' in browser.page_source


def test_submit_publication_valid_with_hashtags(browser_logged_in, data_updater):
    browser = browser_logged_in

    browser.get('http://127.0.0.1:5000/publications/')

    assert 'Nouvelle publication' in browser.page_source

    text_area = browser.find_element_by_id('contenu')
    text_area.send_keys('Meow meow meow')

    hashtag_area = browser.find_element_by_id('hashtags')
    hashtag_area.send_keys('#hashtag')

    submit_button = browser.find_element_by_id('submit')
    submit_button.click()

    assert '#hashtag' in browser.page_source

    data_updater.run_parameterized_query("""\
    DELETE FROM publication
    WHERE contenu='Meow meow meow'""")

    data_updater.run_parameterized_query("""\
    DELETE FROM hashtag
    WHERE nom='#hashtag'""")

