from requests import get
import re
from bs4 import BeautifulSoup as Soup


def flatten_2d(arr):
    return [y for x in arr for y in x]


class WebScraper:
    """Class to facilitate scraping web pages for mock data"""

    def __init__(self, url):
        page = get(url)
        page.raise_for_status()
        page_html = page.text
        self.soup = Soup(page_html, 'lxml')

    def get_content_list(self, html_pattern, regex_pattern):
        tags = self.soup.select(html_pattern)
        regex = re.compile(regex_pattern)
        return flatten_2d([[match for match in regex.findall(''.join([s for s in t.contents if isinstance(s, str)]))] for t in tags])
