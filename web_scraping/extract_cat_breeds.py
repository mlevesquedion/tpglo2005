import re

FILE_NAME = 'cat_breeds.html'

pattern = re.compile(r'<td><a.+>(\w+)</a></td>')

with open('cat_breeds.html') as srce_file,\
        open('cat_breeds.txt', 'w') as dest_file:
    for line in srce_file.readlines():
        match = pattern.match(line)
        if match is not None:
            dest_file.write(match.groups()[0] + '\n')
