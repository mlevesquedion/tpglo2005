from web_scraping.WebScraper import WebScraper
import re

url = 'http://www.catnameslist.com/popularcatnames.html'
scraper = WebScraper(url)
name_regex = re.compile(r'\b\w+\b')
cat_names = scraper.get_content_list('.namecolumn p', name_regex)

with open('cat_names.txt', 'w') as file:
    for cat_name in cat_names:
        file.write(cat_name +'\n')
