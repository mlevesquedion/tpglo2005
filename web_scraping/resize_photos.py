import os
from PIL import Image

SIDE = 100

for filename in os.listdir('photos'):
    filepath = r'photos\{}'.format(filename)
    with Image.open(filepath) as image:
        image_width, image_height = image.size[0], image.size[1]
        minimum_side = min((image_width, image_height))
        ratio = SIDE / minimum_side
        new_width = int(image_width * ratio)
        new_height = int(image_height * ratio)
        image = image.resize((new_width, new_height))
        image = image.crop((0, 0, SIDE, SIDE))
        image = image.convert('RGB')
        image.save(r'squares\{}'.format(filename), 'JPEG')
