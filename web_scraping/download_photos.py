import os
import requests


with open(r'..\thecatapikey.txt') as file:
    api_key = file.read()


DEST_FOLDER = 'photos'
URL = r'http://thecatapi.com/api/images/get?api_key={}'.format(api_key)


for i in range(10, 100):
    page = requests.get(URL)
    page.raise_for_status()

    filepath = r'photos/{}.jpg'.format(i)

    if os.path.exists(filepath):
        print('File already exists!')
        continue

    with open(filepath, 'wb') as image_file:
        for chunk in page.iter_content(100000):
            image_file.write(chunk)

