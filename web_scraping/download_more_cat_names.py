from WebScraper import WebScraper
import re

url = 'https://petset.com/pet-names/popular-cat-names/'

scraper = WebScraper(url)
name_regex = re.compile(r'.+')
cat_names = scraper.get_content_list('li', name_regex)

with open('cat_names.txt', 'w') as file:
    for cat_name in cat_names:
        file.write(cat_name.title() +'\n')