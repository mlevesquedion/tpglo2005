from web_scraping.WebScraper import WebScraper
import re

base_url = r"http://gethashtags.com/search/tag/"
tags = ['kitten', 'cat']

hashtag_regex = re.compile(r'.+')

with open('hashtags.txt', 'w') as hashtag_file:
    for tag in tags:
        scraper = WebScraper(base_url + tag)
        hashtags = scraper.get_content_list('.bluebutton', hashtag_regex)
        for hashtag in hashtags:
            hashtag_file.write(hashtag + '\n')
