import re
from mockers import *


def test_get_sex():
    for _ in range(10):
        sex = get_sex()
        assert sex in ('male', 'female')


def test_get_name():
    name = get_name('male')
    assert name and isinstance(name, str)
    name = get_name('female')
    assert name and isinstance(name, str)


def test_get_paragraph():
    paragraph = get_paragraph()
    assert paragraph and isinstance(paragraph, str)


def test_get_content():
    content = get_content()
    assert content and isinstance(content, str)


def test_get_email():
    email_pattern = r'\w+\d+@(gmail|yahoo|outlook|catmail).com'
    email_regex = re.compile(email_pattern)
    email = get_email(get_name('male'))
    assert email_regex.match(email) is not None


def test_get_date():
    date_pattern = r'\d{4}-\d{2}-\d{2}'
    date_regex = re.compile(date_pattern)
    date = get_date()
    assert date_regex.match(date) is not None


def test_get_date_after():
    for _ in range(1000):
        date = get_date_object()
        date_after = get_date_object(after=date)
        assert date <= date_after


def test_get_timestamp():
    timestamp_pattern = r'\d{4}-\d{2}-\d{2} \d{2}:\d{2}:\d{2}'
    timestamp_regex = re.compile(timestamp_pattern)
    timestamp = get_timestamp()
    assert timestamp_regex.match(timestamp) is not None


def test_get_timestamp_after():
    for _ in range(1000):
        timestamp = get_timestamp_object()
        timestamp_after = get_timestamp_object(after=timestamp)
        assert timestamp <= timestamp_after


def test_get_password():
    password = get_password()
    assert password and isinstance(password, str)


def test_get_hashed_password():
    password = get_hashed_password()
    assert password and isinstance(password, str)


def test_get_breed():
    breed = get_breed()
    assert breed and isinstance(breed, str)


def test_get_hashtag():
    hashtag = get_hashtag()
    assert hashtag and isinstance(hashtag, str)


def test_get_hashtag_list():
    hashtag_list = get_hashtag_list()
    assert all(isinstance(hashtag, str) for hashtag in hashtag_list)


def test_get_all_hashtags():
    all_hashtags = get_all_hashtags()
    assert all_hashtags and isinstance(all_hashtags, list)


def test_get_telephone():
    phone_pattern = r'\d{3} \d{3}-\d{4}'
    phone_regex = re.compile(phone_pattern)
    phone = get_telephone()
    assert phone_regex.match(phone) is not None


def test_get_id_photo():
    id_photo = get_id_photo()
    assert id_photo and isinstance(id_photo, str)


def test_get_bool():
    for _ in range(10):
        bool = get_bool()
        assert bool in (False, True)