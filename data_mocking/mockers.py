import datetime
import hashlib
import random
import re
import os
from string import ascii_letters as allcase_letters
from string import ascii_lowercase as lowercase_letters
from string import digits
from calendar import monthrange


def get_sex():
    """Return a random sex, male or female with equal probabilities"""
    return ['male', 'female'][random.randint(0, 1)]


def get_name(sex):
    """Return a cat name according given a sex"""
    abspath = os.path.abspath(__file__)
    dname = os.path.dirname(abspath)
    os.chdir(dname)
    assert sex.lower() in ('male', 'female')
    with open('{}_cat_names.txt'.format(sex)) as name_file:
        names = name_file.readlines()
        return random.choice(names).strip()


def get_paragraph(n_sentences=1):
    """Return a paragraph of cat ipsum"""
    with open('cat_ipsum.txt') as ipsum_file:
        all_sentences = [line.strip() for line in ipsum_file.readlines()]
        sentence_sample = random.sample(all_sentences, n_sentences)
        return ' '.join(sentence_sample).strip().replace("'", "''")


def get_content(max_lines=10):
    """Return a paragraph containing 1 to 10 lines of cat ipsum"""
    length = random.randint(1, max_lines)
    return get_paragraph(length)


def get_email(name):
    """Return an email given a cat name"""
    providers = ['gmail', 'yahoo', 'outlook', 'catmail']
    email = name + str(random.randint(1, 10000))
    email += '@{}.com'.format(random.choice(providers))
    return email


def get_date_object(after=None):
    """Return a date object, which can be after a specified date"""
    date = None
    if after is not None:
        if isinstance(date, datetime.datetime):
            after = after.date()
        after_year = after.year
        after_month = after.month
        after_day = after.day
        year = random.randint(after_year, 2017)
        if year == after_year:
            month = random.randint(after_month, 12)
            if month == after_month:
                day = random.randint(after_day, list(monthrange(year, month))[1])
            else:
                day = random.randint(1, monthrange(year, month)[1])
        else:
            month = random.randint(1, 12)
            day = random.randint(1, monthrange(year, month)[1])
    else:
        year = random.randint(2000, 2017)
        month = random.randint(1, 12)
        day = random.randint(1, monthrange(year, month)[1])
    date = datetime.date(year, month, day)
    return date


def get_date(after=None):
    """Return a date string, which can be after a specified date"""
    date = get_date_object(after)
    date_pattern = '%Y-%m-%d'
    date_string = date.strftime(date_pattern)
    return date_string


def get_time_object(after=None):
    """Return a time object, which can be after a specified time"""
    if after is not None and isinstance(after, datetime.datetime):
        after_hour = after.hour
        after_minute = after.minute
        after_second = after.second
        hour = random.randint(after_hour, 23)
        if hour == after_hour:
            minute = random.randint(after_minute, 59)
            if minute == after_minute:
                second = random.randint(after_second, 59)
            else:
                second = random.randint(0, 59)
        else:
            minute = random.randint(0, 59)
            second = random.randint(0, 59)
    else:
        hour = random.randint(0, 23)
        minute = random.randint(0, 59)
        second = random.randint(0, 59)
    time = datetime.time(hour, minute, second)
    return time


def get_timestamp_object(after=None):
    """Return a timestamp object, which can be after a specified date"""
    date = get_date_object(after=after)
    time = get_time_object(after=after)
    timestamp = datetime.datetime.combine(date, time)
    return timestamp


def get_timestamp(after=None):
    """Return a timestamp string, which can be after a specified date"""
    timestamp_object = get_timestamp_object(after)
    timestamp_pattern = '%Y-%m-%d %H:%M:%S'
    timestamp_string = timestamp_object.strftime(timestamp_pattern)
    return timestamp_string


def get_password():
    """Return a password composed of letters, digits and special characters"""
    digits = ''.join(str(i) for i in range(10))
    special_characters = '!"/$%?&*()'
    characters = allcase_letters + digits + special_characters
    length = random.randint(8, 12)
    return ''.join(random.choice(characters) for _ in range(length))


def get_hashed_password(password=None):
    """Return a hashed password, using SHA256"""
    if password is None:
        password = get_password()
    password_bytes = bytes(password, encoding='utf-8')
    return hashlib.sha256(password_bytes).hexdigest()


def get_password_combo():
    """Return both a password and its hash"""
    password = get_password()
    hash = get_hashed_password(password)
    return password, hash


def get_breed():
    """Return a randomly chosen cat breed."""
    if random.random() > 0.5:
        return 'Domestic'
    with open('cat_breeds.txt') as breed_file:
        breeds = breed_file.readlines()
        return random.choice(breeds).strip()


def get_telephone():
    """Return a valid Canadian phone number."""
    codes = [
        403, 587, 780, 825,
        236, 250, 604, 778,
        204, 431, 506, 709,
        902, 782, 226, 249,
        289, 343, 365, 416,
        437, 519, 548, 613,
        647, 705, 807, 905,
        418, 438, 450, 514,
        579, 581, 819, 873,
        306, 639, 867
    ]
    first_three = ''.join(str(random.randint(0, 9)) for _ in range(3))
    last_four = ''.join(str(random.randint(0, 9)) for _ in range(4))
    telephone = '{} {}-{}'.format(random.choice(codes), first_three,
                                    last_four)
    return telephone


def get_id_photo():
    """Return a id_photo for use with thecatapi"""
    possible_characters = lowercase_letters + digits
    id = random.choice('23456789abcdef') + ''.join(random.choice(possible_characters) for _ in range(2))
    return id


def get_bool(cutoff=0.5):
    """Return a boolean. Higher cutoff = less likely to get 'True'"""
    return random.random() > cutoff


def get_hashtag():
    """Return a hashtag, randomly chosen from a list of all hashtags"""
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    with open('hashtags.txt') as hashtag_file:
        all_hashtags = [line.strip() for line in hashtag_file.readlines()]
        return random.choice(all_hashtags)


def get_all_hashtags():
    """Return the list of all hashtags"""
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    with open('hashtags.txt') as hashtag_file:
        return [line.strip() for line in hashtag_file.readlines()]


def get_hashtag_list():
    """Return a list of 0 to 10 hashtags"""
    n_hashtags = random.randint(0, 10)
    return [get_hashtag() for _ in range(n_hashtags)]


if __name__ == '__main__':
    pairings = (
        ('Random male name', lambda: get_name('male')),
        ('Random female name', lambda: get_name('female')),
        ('Random sentence', get_paragraph),
        ('Random email', lambda: get_email('Moses')),
        ('Random password', lambda: ' '.join(get_password_combo())),
        ('Random telephone #', get_telephone),
        ('Random breed', get_breed),
        ('Random date', get_date),
        ('Random timestamp', lambda: get_timestamp(get_date_object())),
        ('Random id_photo', get_id_photo),
        ('Random hashtag', get_hashtag)
    )
    for description, function in pairings:
        print(description, ':', function(), end='\n')
