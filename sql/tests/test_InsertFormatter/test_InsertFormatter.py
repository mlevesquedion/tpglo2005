from datetime import datetime
from pytest import raises
from sql.insert.InsertFormatter import InsertFormatter
from sql.insert.InsertFormatter import UnevenNumberOfFields


def test_get_formatted_insert_valid():
    table_name = 'catnet'
    table_fields = ['id', 'name', 'birth_date']
    date = datetime.now()
    date_str = str(date)
    values = [
        [1, 'Michaël', date],
        [2, 'Maxime', date],
        [3, 'Louis-Philippe', date]
    ]
    expected = """\
    INSERT INTO catnet (id,name,birth_date)
    VALUES
    (1,'Michaël',{0}),
    (2,'Maxime',{0}),
    (3,'Louis-Philippe',{0})""".format(date)
    expected = ''.join(expected.split())  # remove all whitespace
    output = InsertFormatter.get_formatted_insert(table_name, table_fields, values)
    output = ''.join(output.split())
    assert expected == output


def test_get_formatted_insert_uneven_lengths():
    table_name = 'catnet'
    table_fields = ['id', 'name']
    date = datetime.now()
    date_str = str(date)
    values = [
        [1, 'Michaël', date],
        [2, 'Maxime', date],
        [3, 'Louis-Philippe', date]
    ]
    with raises(UnevenNumberOfFields):
        InsertFormatter.get_formatted_insert(table_name, table_fields, values)


def test_get_values_string_valid():
    data = [
        ['Chat', 'Chien', 'Souris', 1, 2],
        ['1', '2', '3', 42, 57]
    ]
    expected = """('Chat','Chien','Souris',1,2),('1','2','3',42,57)"""
    output = InsertFormatter.get_values_string(data)
    assert expected == output


def test_get_values_string_uneven_lengths():
    data = [
        ['Chat', 'Chien', 'Souris', 1, 2],
        ['1', '2', '3', 42]
    ]
    with raises(UnevenNumberOfFields):
        InsertFormatter.get_values_string(data)


def test_even_lengths_even():
    data = [
        '123',
        '456'
    ]
    assert InsertFormatter.even_lengths(data)


def test_even_lengths_uneven():
    data = [
        '123',
        '45'
    ]
    assert not InsertFormatter.even_lengths(data)


def test_get_partially_formatted_insert():
    table_name = 'catnet'
    table_fields = ['id', 'name']
    values = [1, 'Michaël']
    expected = """INSERT INTO catnet (id,name) VALUES (%s,%s)"""
    actual = InsertFormatter.get_partially_formatted_insert(table_name, table_fields, values)
    actual = ' '.join(actual.split())
    assert expected == actual
