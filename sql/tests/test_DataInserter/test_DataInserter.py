from datetime import date


def test_everything(data_inserter):
    """A single test case is used, otherwise the time to create a new data_inserter object
    for each test would take too long and make testing impractical"""

    # utilisateur
    user = [
        'name@gmail.com',
        '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',
        'Name',
        '418 123-4567',
        'male',
        'Domestic',
        date.today(),
        'a02'
    ]
    data_inserter.insert_utilisateur(user)
    assert data_inserter.run_parameterized_query("SELECT * FROM utilisateur WHERE nom='Name'")

    # demandeAmitie
    demande = [
        1,
        6  # Watch out, this test depends on the previous one
    ]
    data_inserter.insert_demandeAmitie(demande)
    assert data_inserter.run_parameterized_query("SELECT * FROM demandeAmitie WHERE id_source=1 AND id_cible=6")

    # message
    message = [
        1,
        6,  # Watch out
        "Meow meow meow"
    ]
    data_inserter.insert_message(message)
    assert data_inserter.run_parameterized_query("SELECT * FROM message WHERE id_source=1 AND id_cible=6")

    # publication
    publication = [
        1,
        "Meow meow meow",
        False
    ]
    data_inserter.insert_publication(publication)
    assert data_inserter.run_parameterized_query("SELECT * FROM publication WHERE contenu='Meow meow meow'")

    # publicationAime
    publication_id = data_inserter.run_parameterized_query('SELECT id FROM publication WHERE niveau_acces=TRUE LIMIT 1')[0]['id']
    publicationAime = [
        publication_id,
        6  # Watch out
    ]
    data_inserter.insert_publicationAime(publicationAime)
    assert data_inserter.run_parameterized_query("SELECT * FROM publicationAime WHERE id_publication=%s AND id_utilisateur=6", (publication_id,))

    # publicationCommentaire
    publicationCommentaire = [
        publication_id,  # Watch out
        6,  # Watch out
        "Meow meow meow"
    ]
    data_inserter.insert_publicationCommentaire(publicationCommentaire)
    assert data_inserter.run_parameterized_query("SELECT * FROM publicationCommentaire WHERE contenu='Meow meow meow'")

    # hashtag
    hashtag = ["#meowmeow"]
    data_inserter.insert_hashtag(hashtag)
    assert data_inserter.run_parameterized_query("SELECT * FROM hashtag WHERE nom='#meowmeow'")

    # publicationHashtag
    publicationHashtag = [
        publication_id,  # Watch out
        "#meowmeow"
    ]
    data_inserter.insert_publicationHashtag(publicationHashtag)
    assert data_inserter.run_parameterized_query("SELECT * FROM publicationHashtag WHERE nom_hashtag='#meowmeow'")
