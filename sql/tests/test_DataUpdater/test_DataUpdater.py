def test_everything(data_updater):
    """A single test case is used, otherwise the time to create a new data_fetcher
    object for each test would take too long and make testing impractical"""

    # Insert a new user for use by upcoming tests
    data_updater.run_parameterized_query(
        """\
        INSERT INTO utilisateur
        (email, mot_passe, nom, telephone, sexe, race, date_naissance, instant_inscription, id_photo)
        VALUES
        ('admin2@gmail.com',
        '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',
        'Admin',
        '418 123-4567',
        'male',
        'Domestic',
        CURDATE(),
        CURRENT_TIMESTAMP(),
        'a02')
        """
    )

    # get_formatted_update_list
    dictionary = {'name': 'Admin', 'id': 1}
    update_list = data_updater.get_formatted_update_list(dictionary)
    assert update_list == "name='Admin',id=1"

    # update user with update_generic
    user_id = 1
    before = data_updater.run_parameterized_query(
        'SELECT * FROM utilisateur WHERE id=%s', (user_id))
    dictionary = {'nom': 'Meowmeow', 'telephone': '123 456-7890'}
    data_updater.update_generic(user_id, dictionary, 'utilisateur')
    after = data_updater.run_parameterized_query(
        'SELECT * FROM utilisateur WHERE id=%s', (user_id))
    assert before != after

    # update_user
    user_id = 2
    before = data_updater.run_parameterized_query('SELECT * FROM utilisateur WHERE id=%s', (user_id))
    dictionary = {'nom': 'Meowmeow', 'telephone': '123 456-7890'}
    data_updater.update_user(user_id, dictionary)
    after = data_updater.run_parameterized_query('SELECT * FROM utilisateur WHERE id=%s', (user_id))
    assert before != after

    # accept_friend_request
    id_source, id_cible = 1, 6
    data_updater.run_parameterized_query('INSERT INTO demandeAmitie (id_source, id_cible) VALUES (%s, %s)', (id_source, id_cible))
    data_updater.accept_friend_request(id_source, id_cible)
    etat = data_updater.run_parameterized_query('SELECT etat FROM demandeAmitie WHERE id_source=%s AND id_cible=%s', (id_source, id_cible))[0]['etat']
    assert etat == True

    # change_publication_status
    id_publication = data_updater.run_parameterized_query('SELECT id FROM publication LIMIT 1')[0]['id']
    data_updater.change_publication_access_level(id_publication, False)
    niveau_acces = data_updater.run_parameterized_query('SELECT niveau_acces FROM publication WHERE id=%s', (id_publication,))[0]['niveau_acces']
    assert niveau_acces == False
    data_updater.change_publication_access_level(id_publication, True)
    niveau_acces = data_updater.run_parameterized_query('SELECT niveau_acces FROM publication WHERE id=%s', (id_publication,))[0]['niveau_acces']
    assert niveau_acces == True

    # unfriend
    id_source, id_cible = data_updater.run_parameterized_query('SELECT id_source, id_cible FROM demandeAmitie LIMIT 1')[0].values()
    data_updater.unfriend(id_source, id_cible)
    demande = data_updater.run_parameterized_query('SELECT TRUE FROM demandeAmitie WHERE id_source=%s AND id_cible=%s', (id_source, id_cible))
    assert not demande

    # unlike
    id_utilisateur, id_publication = data_updater.run_parameterized_query('SELECT id_utilisateur, id_publication FROM publicationAime LIMIT 1')[0].values()
    data_updater.unlike(id_utilisateur, id_publication)
    aime = data_updater.run_parameterized_query('SELECT TRUE FROM publicationAime WHERE id_utilisateur=%s AND id_publication=%s',(id_utilisateur, id_publication))
    assert not aime

    # delete_account_by_id
    id_utilisateur = data_updater.run_parameterized_query('SELECT id FROM utilisateur LIMIT 1')[0]['id']
    data_updater.delete_account_by_id(id_utilisateur)
    utilisateur = data_updater.run_parameterized_query('SELECT TRUE FROM utilisateur WHERE id=%s', (id_utilisateur,))
    assert not utilisateur

    # delete_account_by_id
    email_utilisateur = \
    data_updater.run_parameterized_query('SELECT email FROM utilisateur LIMIT 1')[
        0]['email']
    data_updater.delete_account_by_email(email_utilisateur)
    utilisateur = data_updater.run_parameterized_query(
        'SELECT TRUE FROM utilisateur WHERE email=%s', (email_utilisateur,))
    assert not utilisateur
