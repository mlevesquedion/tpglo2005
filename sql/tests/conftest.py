import pytest

from sql.DataFetcher import DataFetcher
from sql.DataUpdater import DataUpdater
from sql.DataInserter import DataInserter
from sql.setup_database import setup_database


@pytest.fixture()
def db_conn():

    test_db_name = 'catnet_test'

    db_conn = setup_database(test_db_name, 0, with_admin=False, ask_before_reset=False)

    yield db_conn

    db_conn.close()


@pytest.fixture()
def db_conn_no_util():

    test_db_name = 'catnet_test'

    db_conn = setup_database(test_db_name, 0, with_admin=False, ask_before_reset=False)

    db_conn.run_query('DROP TRIGGER friendRequestAccepted', with_commit=True)

    yield db_conn

    db_conn.close()


@pytest.fixture()
def data_fetcher():

    test_db_name = 'catnet_test'

    db_conn = setup_database(test_db_name, 5, with_admin=True, ask_before_reset=False)

    data_fetcher = DataFetcher(test_db_name)

    yield data_fetcher

    db_conn.close()


@pytest.fixture()
def data_updater():

    test_db_name = 'catnet_test'

    db_conn = setup_database(test_db_name, 5, with_admin=False, ask_before_reset=False)

    data_updater = DataUpdater(test_db_name)

    yield data_updater

    db_conn.close()


@pytest.fixture()
def data_inserter():

    test_db_name = 'catnet_test'

    db_conn = setup_database(test_db_name, 5, with_admin=False, ask_before_reset=False)

    data_inserter = DataInserter(test_db_name)

    yield data_inserter

    db_conn.close()