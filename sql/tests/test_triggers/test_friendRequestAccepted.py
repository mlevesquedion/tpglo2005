from sql.insert.tables.utilisateur import insert_users


def test_friend_request_accepted(db_conn):
    insert_users(db_conn, 2)
    insert_query = """\
        INSERT INTO demandeAmitie
        (id_source, id_cible, instant, etat)
        VALUES
        (1, 2, CURRENT_TIMESTAMP(), FALSE)"""
    update_query = """\
        UPDATE demandeAmitie
        SET etat = TRUE
        WHERE id_source = 1 AND id_cible = 2"""
    select_query = """\
    SELECT TRUE
    FROM amitie
    WHERE id_demande = (
      SELECT id
      FROM demandeAmitie
      WHERE id_source = 1 AND id_cible = 2
    )"""
    db_conn.run_query(insert_query)
    db_conn.run_query(update_query)
    result = db_conn.run_query(select_query)
    assert (True,) in result
