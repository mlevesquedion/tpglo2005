from pytest import raises
from sql.insert.tables.utilisateur import insert_users
from pymysql.err import InternalError


def test_valid_publication(db_conn):
    insert_users(db_conn, 1)
    query = """\
        INSERT INTO publication
        (id_utilisateur, instant, contenu, niveau_acces)
        VALUES
        (1, CURRENT_TIMESTAMP(), "Meow", TRUE)
        """
    db_conn.run_query(query)


def test_invalid_publication_empty(db_conn):
    insert_users(db_conn, 1)
    query = """\
        INSERT INTO publication
        (id_utilisateur, instant, contenu, niveau_acces)
        VALUES
        (1, CURRENT_TIMESTAMP(), "", FALSE)
        """
    with raises(InternalError):
        db_conn.run_query(query)


def test_invalid_publication_before_join_time(db_conn):
    insert_users(db_conn, 1)
    query = """\
        INSERT INTO publication
        (id_utilisateur, instant, contenu, niveau_acces)
        VALUES
        (1, (SELECT instant_inscription - INTERVAL 1 SECOND FROM utilisateur WHERE id = 1), "Meow", FALSE)
        """
    with raises(InternalError):
        db_conn.run_query(query)
