from pytest import raises
from sql.insert.tables.utilisateur import insert_users
from pymysql.err import InternalError


def test_valid_friend_request(db_conn):
    insert_users(db_conn, 2)
    query = """\
        INSERT INTO demandeAmitie
        (id_source, id_cible, instant, etat)
        VALUES
        (1, 2, CURRENT_TIMESTAMP(), FALSE)
        """
    db_conn.run_query(query)


def test_invalid_friend_request_before_join_dates(db_conn):
    insert_users(db_conn, 2)
    query = """\
        INSERT INTO demandeAmitie
        (id_source, id_cible, instant, etat)
        VALUES
        (1, 2, CURRENT_TIMESTAMP() - INTERVAL 20 YEAR, FALSE)
        """
    with raises(InternalError):
        db_conn.run_query(query)
