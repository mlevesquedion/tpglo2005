from pytest import raises
from sql.insert.tables.utilisateur import insert_users
from pymysql.err import IntegrityError


def test_valid_message(db_conn):
    insert_users(db_conn, 2)
    preparation_queries = [
        """\
        INSERT INTO demandeAmitie
        (id_source, id_cible, instant, etat)
        VALUES
        (1, 2, CURRENT_TIMESTAMP() - INTERVAL 1 HOUR, FALSE)"""
        ,
        """\
        UPDATE demandeAmitie
        SET etat = TRUE
        WHERE id = 1"""
    ]
    for query in preparation_queries:
        db_conn.run_query(query)

    query = """\
        INSERT INTO message
        (id_source, id_cible, instant, contenu)
        VALUES
        (1, 2, CURRENT_TIMESTAMP() + INTERVAL 1 SECOND, "Meow")
        """
    db_conn.run_query(query)


def test_invalid_message_not_friends(db_conn):
    insert_users(db_conn, 3)

    query = """\
        INSERT INTO message
        (id_source, id_cible, instant, contenu)
        VALUES
        (1, 3, CURRENT_TIMESTAMP() + INTERVAL 1 SECOND, "Meow")
        """
    with raises(IntegrityError):
        db_conn.run_query(query)


def test_invalid_message_empty(db_conn):
    insert_users(db_conn, 2)
    preparation_queries = [
        """\
        INSERT INTO demandeAmitie
        (id_source, id_cible, instant, etat)
        VALUES
        (1, 2, CURRENT_TIMESTAMP(), FALSE)"""
        ,
        """\
        UPDATE demandeAmitie
        SET etat = TRUE
        WHERE id = 1"""
    ]
    for query in preparation_queries:
        db_conn.run_query(query)

    query = """\
        INSERT INTO message
        (id_source, id_cible, instant, contenu)
        VALUES
        (1, 2, CURRENT_TIMESTAMP() + INTERVAL 1 SECOND, "")
        """
    with raises(IntegrityError):
        db_conn.run_query(query)


def test_invalid_message_before_friendship_start_date(db_conn):
    insert_users(db_conn, 2)
    preparation_queries = [
        """\
        INSERT INTO demandeAmitie
        (id_source, id_cible, instant, etat)
        VALUES
        (1, 2, CURRENT_TIMESTAMP(), FALSE)"""
        ,
        """\
        UPDATE demandeAmitie
        SET etat = TRUE
        WHERE id = 1"""
    ]
    for query in preparation_queries:
        db_conn.run_query(query)

    query = """\
        INSERT INTO message
        (id_source, id_cible, instant, contenu)
        VALUES
        (1, 2, CURRENT_TIMESTAMP() - INTERVAL 1 SECOND, "Meow")
        """
    with raises(IntegrityError):
        db_conn.run_query(query)
