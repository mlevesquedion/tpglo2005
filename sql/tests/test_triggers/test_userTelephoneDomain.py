from pytest import raises
from pymysql.err import InternalError


def test_valid_telephone(db_conn):
    query = """\
        INSERT INTO utilisateur
        (email, mot_passe, nom, telephone, sexe, race, date_naissance, 
        instant_inscription, id_photo)
        VALUES
        ('admin2@gmail.com',
        'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
        'Abc',
        
        '123 456-7890',
        
        'male',
        'Domestic',
        CURDATE(),
        CURRENT_TIMESTAMP(),
        'a02')
        """
    db_conn.run_query(query)


def test_too_short_telephone(db_conn):
    query = """\
        INSERT INTO utilisateur
        (email, mot_passe, nom, telephone, sexe, race, date_naissance, 
        instant_inscription, id_photo)
        VALUES
        ('admin2@gmail.com',
        'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
        'Abc',

        '123 456-789',

        'male',
        'Domestic',
        CURDATE(),
        CURRENT_TIMESTAMP(),
        'a02')
        """
    with raises(InternalError):
        db_conn.run_query(query)


def test_invalid_telephone(db_conn):
    query = """\
        INSERT INTO utilisateur
        (email, mot_passe, nom, telephone, sexe, race, date_naissance, 
        instant_inscription, id_photo)
        VALUES
        ('admin2@gmail.com',
        'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
        'Abc',

        '123 456-789A',

        'male',
        'Domestic',
        CURDATE(),
        CURRENT_TIMESTAMP(),
        'a02')
        """
    with raises(InternalError):
        db_conn.run_query(query)