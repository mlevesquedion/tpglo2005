from pytest import raises
from sql.insert.tables.utilisateur import insert_users
from pymysql.err import IntegrityError


def test_valid_comment_public(db_conn):
    insert_users(db_conn, 2)
    preparation_queries = [
        """\
        INSERT INTO publication
        (id_utilisateur, instant, contenu, niveau_acces)
        VALUES
        (1, CURRENT_TIMESTAMP(), 'Meow', TRUE)"""
    ]
    for query in preparation_queries:
        db_conn.run_query(query)

    query = """\
        INSERT INTO publicationCommentaire
        (id_publication, id_utilisateur, instant, contenu)
        VALUES
        (1, 2, CURRENT_TIMESTAMP() + INTERVAL 1 SECOND, "Meow")
        """
    db_conn.run_query(query)


def test_valid_comment_friend(db_conn):
    insert_users(db_conn, 2)
    preparation_queries = [
        """\
        INSERT INTO demandeAmitie
        (id_source, id_cible, instant, etat)
        VALUES
        (1, 2, CURRENT_TIMESTAMP() - INTERVAL 1 HOUR, FALSE)"""
        ,
        """\
        UPDATE demandeAmitie
        SET etat = TRUE
        WHERE id = 1"""
        ,
        """\
        INSERT INTO publication
        (id_utilisateur, instant, contenu, niveau_acces)
        VALUES
        (1, CURRENT_TIMESTAMP(), 'Meow', FALSE)"""
    ]
    for query in preparation_queries:
        db_conn.run_query(query)

    query = """\
        INSERT INTO publicationCommentaire
        (id_publication, id_utilisateur, instant, contenu)
        VALUES
        (1, 2, CURRENT_TIMESTAMP() + INTERVAL 1 SECOND, "Meow")
        """
    db_conn.run_query(query)


def test_invalid_comment_not_friend(db_conn):
    insert_users(db_conn, 2)
    preparation_queries = [
        """\
        INSERT INTO publication
        (id_utilisateur, instant, contenu, niveau_acces)
        VALUES
        (1, CURRENT_TIMESTAMP(), 'Meow', FALSE)"""
    ]
    for query in preparation_queries:
        db_conn.run_query(query)

    query = """\
        INSERT INTO publicationCommentaire
        (id_publication, id_utilisateur, instant, contenu)
        VALUES
        (1, 2, CURRENT_TIMESTAMP() + INTERVAL 1 SECOND, "Meow")
        """
    with raises(IntegrityError):
        db_conn.run_query(query)


def test_invalid_comment_empty(db_conn):
    insert_users(db_conn, 1)
    preparation_queries = [
        """\
        INSERT INTO publication
        (id_utilisateur, instant, contenu, niveau_acces)
        VALUES
        (1, CURRENT_TIMESTAMP(), 'Meow', TRUE)"""
    ]
    for query in preparation_queries:
        db_conn.run_query(query)
    query = """\
        INSERT INTO publicationCommentaire
        (id_publication, id_utilisateur, instant, contenu)
        VALUES
        (1, 1, CURRENT_TIMESTAMP() + INTERVAL 1 SECOND, "")
        """
    with raises(IntegrityError):
        db_conn.run_query(query)


def test_invalid_comment_before_publication(db_conn):
    insert_users(db_conn, 1)
    preparation_queries = [
        """\
        INSERT INTO publication
        (id_utilisateur, instant, contenu, niveau_acces)
        VALUES
        (1, CURRENT_TIMESTAMP(), 'Meow', TRUE)"""
    ]
    for query in preparation_queries:
        db_conn.run_query(query)

    query = """\
        INSERT INTO publicationCommentaire
        (id_publication, id_utilisateur, instant, contenu)
        VALUES
        (1, 1, CURRENT_TIMESTAMP() - INTERVAL 1 SECOND, "Meow")
        """
    with raises(IntegrityError):
        db_conn.run_query(query)