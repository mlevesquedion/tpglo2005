from pytest import raises
from pymysql.err import InternalError


def test_friend_self_fails(db_conn):
    query = """\
        INSERT INTO demandeAmitie
        (id_source, id_cible, instant, etat)
        VALUES
        (1, 1, CURRENT_TIMESTAMP(), FALSE)"""
    with raises(InternalError):
        db_conn.run_query(query)