from pytest import raises
from pymysql.err import IntegrityError


def test_valid_hashtag(db_conn_no_util):
    query = """\
        INSERT INTO hashtag
        (nom)
        VALUES
        ("#abc")"""
    db_conn_no_util.run_query(query)


def test_invalid_hashtag_too_short(db_conn_no_util):
    query = """\
            INSERT INTO hashtag
            (nom)
            VALUES
            ("#ab")"""
    with raises(IntegrityError):
        db_conn_no_util.run_query(query)


def test_invalid_hashtag_no_hashtag(db_conn_no_util):
    query = """\
            INSERT INTO hashtag
            (nom)
            VALUES
            ("abcd")"""
    with raises(IntegrityError):
        db_conn_no_util.run_query(query)


def test_invalid_hashtag_not_lowercase(db_conn_no_util):
    query = """\
            INSERT INTO hashtag
            (nom)
            VALUES
            ("#Abc")"""
    with raises(IntegrityError):
        db_conn_no_util.run_query(query)