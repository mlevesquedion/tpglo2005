from pytest import raises
from pymysql.err import InternalError


def test_valid_password(db_conn):
    query = """\
        INSERT INTO utilisateur
        (email, mot_passe, nom, telephone, sexe, race, date_naissance, 
        instant_inscription, id_photo)
        VALUES
        ('admin2@gmail.com',
        
        'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',
        
        'Abc',
        '418 123-4567',
        'male',
        'Domestic',
        CURDATE(),
        CURDATE(),
        'a02')
        """
    db_conn.run_query(query)


def test_too_short_password(db_conn):
    query = """\
        INSERT INTO utilisateur
        (email, mot_passe, nom, telephone, sexe, race, date_naissance, 
        instant_inscription, id_photo)
        VALUES
        ('admin2@gmail.com',

        'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa',

        'Abc',
        '418 123-4567',
        'male',
        'Domestic',
        CURDATE(),
        CURRENT_TIMESTAMP(),
        'a02')
        """
    with raises(InternalError):
        db_conn.run_query(query)


def test_invalid_character_password(db_conn):
    query = """\
        INSERT INTO utilisateur
        (email, mot_passe, nom, telephone, sexe, race, date_naissance, 
        instant_inscription, id_photo)
        VALUES
        ('admin2@gmail.com',

        'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaag',

        'Abc',
        '418 123-4567',
        'male',
        'Domestic',
        CURDATE(),
        CURRENT_TIMESTAMP(),
        'a02')
        """
    with raises(InternalError):
        db_conn.run_query(query)