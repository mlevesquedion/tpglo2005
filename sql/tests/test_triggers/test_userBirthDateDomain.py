from pytest import raises
from pymysql.err import InternalError


def test_valid_user_birthdate(db_conn):
    query = """\
        INSERT INTO utilisateur
        (email, mot_passe, nom, telephone, sexe, race, date_naissance, instant_inscription, id_photo)
        VALUES
        ('admin2@gmail.com',
        '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',
        'Admin',
        '418 123-4567',
        'male',
        'Domestic',
        
        CURDATE(),
        CURRENT_TIMESTAMP(),
        
        'a02')
        """
    db_conn.run_query(query)


def test_invalid_user_birthdate(db_conn):
    query = """\
        INSERT INTO utilisateur
        (email, mot_passe, nom, telephone, sexe, race, date_naissance, instant_inscription, id_photo)
        VALUES
        ('admin2@gmail.com',
        '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',
        'Admin',
        '418 123-4567',
        'male',
        'Domestic',
        
        CURDATE() + INTERVAL 1 DAY,
        CURRENT_TIMESTAMP(),
        
        'a02')
        """
    with raises(InternalError):
        db_conn.run_query(query)
