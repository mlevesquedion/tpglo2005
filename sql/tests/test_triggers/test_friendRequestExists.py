from sql.insert.tables.utilisateur import insert_users
from pytest import raises
from pymysql.err import IntegrityError


def test_friend_request_exists(db_conn):
    insert_users(db_conn, 2)

    valid_insert_query = """\
        INSERT INTO demandeAmitie
        (id_source, id_cible, instant, etat)
        VALUES
        (1, 2, CURRENT_TIMESTAMP(), FALSE)"""
    db_conn.run_query(valid_insert_query)

    invalid_insert_query = """\
            INSERT INTO demandeAmitie
            (id_source, id_cible, instant, etat)
            VALUES
            (2, 1, CURRENT_TIMESTAMP(), FALSE)"""
    with raises(IntegrityError):
        db_conn.run_query(invalid_insert_query)

