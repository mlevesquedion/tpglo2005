"""
These tests indirectly test the MockInserter class, since it is used by all
insertion functions.
For now, all tests are in a single function, since there are intrinsic
dependencies in the data.
"""


from data_mocking.mockers import get_all_hashtags
from sql.insert.insert_all import insert_all

count_query = "SELECT COUNT(*) FROM {}"


def test_inserts(db_conn_no_util):

    db_conn = db_conn_no_util

    n_users_to_insert = 10

    insert_all(db_conn, n_users_to_insert)

    # utilisateur
    user_count_query = count_query.format('utilisateur')
    user_count_after_insert = db_conn.run_query(user_count_query)[0][0]
    assert n_users_to_insert == user_count_after_insert

    # demandeAmitie
    request_count_query = count_query.format('demandeAmitie')
    request_count_after_insert = db_conn.run_query(request_count_query)[0][0]
    assert request_count_after_insert > 0

    # amitie
    friendship_count_query = count_query.format('amitie')
    accepted_friend_request_query = """\
       SELECT id, instant
       FROM demandeAmitie
       WHERE etat = TRUE"""
    accepted_friend_requests = db_conn.run_query(accepted_friend_request_query)
    n_friendships_to_insert = len(accepted_friend_requests)
    friendship_count_after_insert = db_conn.run_query(friendship_count_query)[0][0]
    assert n_friendships_to_insert == friendship_count_after_insert

    # hashtag
    hashtag_count_query = count_query.format('hashtag')
    n_hashtags_to_insert = len(get_all_hashtags())
    hashtag_count_after_insert = db_conn.run_query(hashtag_count_query)[0][0]
    assert n_hashtags_to_insert == hashtag_count_after_insert

    # publication
    publication_count_query = count_query.format('publication')
    publication_count_after_insert = db_conn.run_query(publication_count_query)[0][0]
    assert publication_count_after_insert > 0

    # publicationAime
    publicationAime_count_query = count_query.format('publicationAime')
    publicationAime_count_after_insert = db_conn.run_query(publicationAime_count_query)[0][0]
    assert publicationAime_count_after_insert > 0

    # publicationCommentaire
    publicationCommentaire_count_query = count_query.format('publicationCommentaire')
    publicationCommentaire_count_after_insert = db_conn.run_query(publicationCommentaire_count_query)[0][0]
    assert publicationCommentaire_count_after_insert > 0

    # publicationHashtag
    publicationHashtag_count_query = count_query.format('publicationHashtag')
    publicationHashtag_count_after_insert = db_conn.run_query(publicationHashtag_count_query)[0][0]
    assert publicationHashtag_count_after_insert > 0

    # message
    message_count_query = count_query.format('message')
    message_count_after_insert = db_conn.run_query(message_count_query)[0][0]
    assert message_count_after_insert > 0
