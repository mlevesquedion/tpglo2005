from operator import itemgetter


def test_everything(data_fetcher):
    """Since all methods on data_fetcher are non-mutating, the tests are independent.
    A single test case is used, otherwise the time to create a new data_fetcher object
    for each test would take too long and make testing impractical"""

    # get_formatted_select
    formatted_select = data_fetcher.get_formatted_select('id', ['*'])
    assert formatted_select == '''SELECT *\nFROM utilisateur\nWHERE id = %s'''

    # get_password_by_email
    password_by_email = data_fetcher.get_password_by_email('admin@gmail.com')
    assert password_by_email == '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918'

    # get_user_data_by_email
    user_data = data_fetcher.get_user_data_by_email('admin@gmail.com')
    assert list(user_data.keys()) == ['id', 'email', 'mot_passe', 'nom', 'telephone', 'sexe', 'race', 'date_naissance', 'instant_inscription', 'id_photo']
    assert user_data['id'] == 1

    # get_user_data_by_id
    user_data = data_fetcher.get_user_data_by_id(1)
    assert list(user_data.keys()) == ['id', 'email', 'mot_passe', 'nom', 'telephone', 'sexe', 'race', 'date_naissance', 'instant_inscription', 'id_photo']
    assert user_data['email'] == 'admin@gmail.com'

    # get_user_friend_ids
    user_1_friend_ids = list(data_fetcher.get_user_friend_ids(1))
    for user_id in user_1_friend_ids:
        assert 1 in data_fetcher.get_user_friend_ids(user_id)

    # get_user_friend_count
    user_1_friend_count = data_fetcher.get_user_friend_count(1)
    assert user_1_friend_count == len(user_1_friend_ids)

    # get_users_data_by_name
    users_by_name = data_fetcher.get_users_data_by_name('Admin')
    assert list(users_by_name[0].keys()) == ['id', 'email', 'mot_passe', 'nom', 'telephone', 'sexe', 'race', 'date_naissance', 'instant_inscription', 'id_photo']
    assert users_by_name[0]['nom'] == 'Admin'

    # get_publications_by_user
    publications_by_user = data_fetcher.get_publications_by_user(1)
    assert all(p['id_auteur'] == 1 for p in publications_by_user)

    # get_publications_for_user
    publications_for_user = data_fetcher.get_publications_for_user(1)
    assert all(p['id_auteur'] == 1
               or p['id_auteur'] in user_1_friend_ids
               or p['niveau_acces']
               for p in publications_for_user)

    # get_messages_between_users
    for user_id in user_1_friend_ids:
        messages_between_users = data_fetcher.get_messages_between_users(1, user_id)
        assert all(m['id_source'] in (1, user_id) and
                   m['id_cible'] in (1, user_id)
                   for m in messages_between_users)
        assert list(messages_between_users) == sorted(messages_between_users, key=itemgetter('instant'))
