CREATE TRIGGER friendRequestAccepted
AFTER UPDATE ON demandeAmitie
FOR EACH ROW
BEGIN
  INSERT INTO amitie
  SET id_demande = NEW.id, instant_debut = CURRENT_TIMESTAMP();
END;