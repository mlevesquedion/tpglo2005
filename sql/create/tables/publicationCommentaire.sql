CREATE TABLE IF NOT EXISTS publicationCommentaire (
  id_publication INT,
  id_utilisateur INT,
  instant TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  contenu TEXT NOT NULL,
  PRIMARY KEY (id_publication, id_utilisateur, instant),
  FOREIGN KEY (id_publication) REFERENCES publication (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (id_utilisateur) REFERENCES utilisateur (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
)