CREATE TABLE IF NOT EXISTS message (
  id_source INT,
  id_cible INT,
  instant TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
  contenu TEXT NOT NULL,
  PRIMARY KEY (id_source, id_cible, instant),
  FOREIGN KEY (id_source) REFERENCES utilisateur (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (id_cible) REFERENCES utilisateur (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
)