CREATE TABLE IF NOT EXISTS amitie (
  id_demande INT,
  instant_debut TIMESTAMP NOT NULL,
  PRIMARY KEY (id_demande),
  FOREIGN KEY (id_demande) REFERENCES demandeAmitie (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
)