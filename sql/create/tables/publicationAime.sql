CREATE TABLE IF NOT EXISTS publicationAime (
  id_publication INT,
  id_utilisateur INT,
  instant TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (id_publication, id_utilisateur),
  FOREIGN KEY (id_publication) REFERENCES publication (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (id_utilisateur) REFERENCES utilisateur (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE
)