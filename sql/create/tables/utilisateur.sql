CREATE TABLE IF NOT EXISTS utilisateur (
  id INT AUTO_INCREMENT,
  email VARCHAR(100) NOT NULL,
  mot_passe CHAR(64) NOT NULL,
  nom VARCHAR(20) NOT NULL,
  telephone CHAR(12) NOT NULL,
  sexe ENUM('male', 'female'),
  race ENUM('Abyssinian', 'Aegean', 'Asian', 'Balinese', 'Bambino', 'Bengal', 'Birman', 'Bombay', 'Burmese', 'Burmilla', 'Chartreux', 'Chausie', 'Cheetoh', 'Cyprus', 'Domestic', 'Highlander', 'Javanese', 'Korat', 'LaPerm', 'Lykoi', 'Manx', 'Minskin', 'Munchkin', 'Nebelung', 'Napoleon', 'Ocicat', 'Peterbald', 'Raas', 'Ragamuffin', 'Ragdoll', 'Savannah', 'Serengeti', 'Siamese', 'Siberian', 'Singapura', 'Snowshoe', 'Sokoke', 'Somali', 'Sphynx', 'Suphalak', 'Thai', 'Tonkinese', 'Toyger'),
  date_naissance DATE NOT NULL,
  instant_inscription TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  id_photo CHAR(3) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE (email)
)