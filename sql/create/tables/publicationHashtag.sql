CREATE TABLE IF NOT EXISTS publicationHashtag (
  id_publication INT,
  nom_hashtag VARCHAR(40),
  PRIMARY KEY (id_publication, nom_hashtag),
  FOREIGN KEY (id_publication) REFERENCES publication (id)
  ON DELETE CASCADE
  ON UPDATE CASCADE,
  FOREIGN KEY (nom_hashtag) REFERENCES hashtag (nom)
  ON DELETE CASCADE
  ON UPDATE CASCADE
)