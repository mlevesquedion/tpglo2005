from sql.create.create_from_folder import create_from_folder
from functools import partial

VIEWS_PATH = r'views'

create_views = partial(create_from_folder, VIEWS_PATH)
