CREATE INDEX publicationId
USING HASH
ON publication (id)