from sql.create.create_tables import create_tables
from sql.create.create_triggers import create_domain_triggers
from sql.create.create_views import create_views
from sql.create.create_indexes import create_indexes
from sql.create.generate_update_triggers import generate_update_triggers


def create_all(db_conn):
    create_tables(db_conn)
    create_views(db_conn)
    create_indexes(db_conn)
    generate_update_triggers()
    create_domain_triggers(db_conn)
