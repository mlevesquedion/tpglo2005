CREATE TRIGGER validLike_UPDATE
BEFORE UPDATE ON publicationAime
FOR EACH ROW
BEGIN
  IF NEW.instant < (SELECT instant FROM publication WHERE id = NEW.id_publication)
  OR (
       (SELECT niveau_acces FROM publication WHERE id = NEW.id_publication) = FALSE
       AND NEW.id_utilisateur != (SELECT id_utilisateur FROM publication WHERE id = NEW.id_publication)
       AND NEW.id_utilisateur NOT IN
    (
      SELECT id_source
      FROM amitieComplet
      WHERE id_source = NEW.id_utilisateur AND id_cible = (SELECT id_utilisateur FROM publication WHERE id = NEW.id_publication)
      UNION
      SELECT id_cible
      FROM amitieComplet
      WHERE id_cible = NEW.id_utilisateur AND id_source = (SELECT id_utilisateur FROM publication WHERE id = NEW.id_publication)
    )
  )
    THEN SET NEW.id_publication = NULL;
  END IF;
END;