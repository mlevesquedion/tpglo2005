CREATE TRIGGER validMessage_UPDATE
BEFORE UPDATE ON message
FOR EACH ROW
BEGIN
  IF CHAR_LENGTH(NEW.contenu) = 0
  OR NOT EXISTS (
      SELECT id FROM demandeAmitie WHERE (id_source = NEW.id_source AND id_cible = NEW.id_cible) OR (id_source = NEW
      .id_cible AND id_cible = NEW.id_source)
  )
  OR NEW.instant < (
    SELECT MAX(instant_debut) FROM
      (SELECT instant_debut
       FROM amitieComplet
       WHERE
         (id_source = NEW.id_source AND id_cible = NEW.id_cible)
         OR (id_source = NEW.id_cible AND id_cible = NEW.id_source)
      ) AS date_table
  )
  THEN SET NEW.id_source = NULL;
  END IF;
END;