CREATE TRIGGER notFriendSelfDemande_UPDATE
BEFORE UPDATE ON demandeAmitie
FOR EACH ROW
BEGIN
  IF (NEW.id_source = NEW.id_cible)
    THEN SET NEW.id = 'NULL';
  END IF;
END;