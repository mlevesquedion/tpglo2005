CREATE TRIGGER userEmailDomain_UPDATE
BEFORE UPDATE ON utilisateur
FOR EACH ROW
BEGIN
  IF NOT NEW.email REGEXP '[[:alnum:]]+@[[:alnum:]]+\.(com|ca|fr)'
  THEN SET NEW.id = 'NULL';
  END IF;
END;