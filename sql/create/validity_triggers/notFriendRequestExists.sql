CREATE TRIGGER notFriendRequestExists
BEFORE INSERT ON demandeAmitie
FOR EACH ROW
BEGIN
  IF (SELECT TRUE FROM demandeAmitie WHERE id_source=NEW.id_cible AND id_cible=NEW.id_source)
    THEN SET NEW.id_source = NULL;
  END IF;
END;