CREATE TRIGGER userBirthDateDomain_UPDATE
BEFORE UPDATE ON utilisateur
FOR EACH ROW
BEGIN
  IF NEW.date_naissance > NEW.instant_inscription
  THEN set NEW.id = 'NULL';
  END IF;
END;