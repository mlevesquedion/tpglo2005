CREATE TRIGGER validPublication
BEFORE INSERT ON publication
FOR EACH ROW
BEGIN
  IF CHAR_LENGTH(NEW.contenu) = 0
  OR NEW.instant < (SELECT instant_inscription FROM utilisateur WHERE id = NEW.id_utilisateur)
    THEN SET NEW.id = 'NULL';
  END IF;
END;