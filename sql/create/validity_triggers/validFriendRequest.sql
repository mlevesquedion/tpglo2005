CREATE TRIGGER validFriendRequest
BEFORE INSERT ON demandeAmitie
FOR EACH ROW
BEGIN
  IF NEW.instant < (SELECT instant_inscription FROM utilisateur WHERE id = NEW.id_source)
    OR NEW.instant < (SELECT instant_inscription FROM utilisateur WHERE id = NEW.id_cible)
  THEN SET NEW.id = 'NULL';
  END IF;
END;