CREATE TRIGGER userTelephoneDomain
BEFORE INSERT ON utilisateur
FOR EACH ROW
BEGIN
  IF NOT NEW.telephone REGEXP('^[0-9]{3} [0-9]{3}-[0-9]{4}$')
  THEN set NEW.id = 'NULL';
  END IF;
END;