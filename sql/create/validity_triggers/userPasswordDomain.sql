CREATE TRIGGER userPasswordDomain
BEFORE INSERT ON utilisateur
FOR EACH ROW
BEGIN
  IF CHAR_LENGTH(NEW.mot_passe) != 64
    OR NOT NEW.mot_passe REGEXP '^[a-f|0-9]+$'
  THEN SET NEW.id = 'NULL';
  END IF;
END;