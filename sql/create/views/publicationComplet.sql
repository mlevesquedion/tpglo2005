CREATE VIEW publicationComplet AS
SELECT
  p.id AS id_publication,
  p.instant as instant_publication,
  u.id AS id_auteur,
  u.nom AS nom_auteur,
  u.id_photo AS id_photo,
  p.contenu AS contenu,
  p.niveau_acces AS niveau_acces,
  (SELECT COUNT(*) FROM publicationAime WHERE id_publication = p.id) AS nombre_aime,
  (SELECT COUNT(*) FROM publicationCommentaire WHERE id_publication = p.id) AS nombre_commentaire,
  (SELECT GROUP_CONCAT(nom_hashtag SEPARATOR ',') FROM publicationHashtag WHERE id_publication = p.id GROUP BY
    id_publication) AS liste_hashtags
FROM
  utilisateur u
  JOIN publication p
  ON u.id = p.id_utilisateur
