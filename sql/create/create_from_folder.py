import os


def create_from_folder(folder_name, db_conn):
    os.chdir(os.path.dirname(os.path.abspath(__file__)))

    print('\nCREATING {}'.format(folder_name.upper()))

    for file_name in os.listdir(folder_name):

        file_path = os.path.join(folder_name, file_name)

        with open(file_path) as file:
            print('--Creating {}'.format(file_name[:-4]))
            query = file.read()
            db_conn.run_query(query, with_commit=True)