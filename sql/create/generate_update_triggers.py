import os


def generate_update_triggers():
    print('\nGENERATING UPDATE TRIGGERS')

    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    triggers_path = r'validity_triggers'

    for filename in os.listdir(triggers_path):
        filepath = os.path.join(triggers_path, filename)
        with open(filepath) as trigger_file:
            contents = trigger_file.read()
            if 'BEFORE INSERT' in contents:
                trigger_name = filename[:-4]
                new_trigger_name = trigger_name + '_UPDATE'
                update_contents = contents.replace('BEFORE INSERT', 'BEFORE UPDATE')
                update_contents = update_contents.replace(trigger_name, new_trigger_name)
                update_filepath = os.path.join(triggers_path, new_trigger_name + '.sql')
                with open(update_filepath, 'w') as update_trigger_file:
                    update_trigger_file.write(update_contents)


if __name__ == '__main__':
    generate_update_triggers()
