from sql.create.create_from_folder import create_from_folder
from functools import partial

INDEXES_PATH = r'indexes'

create_indexes = partial(create_from_folder, INDEXES_PATH)
