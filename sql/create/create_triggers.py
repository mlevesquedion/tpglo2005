from sql.create.create_from_folder import create_from_folder
from functools import partial

DOMAIN_TRIGGERS_PATH = r'validity_triggers'
UTILITY_TRIGGERS_PATH = r'utility_triggers'

create_domain_triggers = partial(create_from_folder, DOMAIN_TRIGGERS_PATH)
create_utility_triggers = partial(create_from_folder, UTILITY_TRIGGERS_PATH)
