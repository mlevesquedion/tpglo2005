from sql.db_utils import ordered_table_names
import os

os.chdir(os.path.dirname(os.path.abspath(__file__)))

CREATE_TABLES_PATH = r'tables'


def create_tables(DBconn):
    os.chdir(os.path.dirname(os.path.abspath(__file__)))

    print('\nCREATING TABLES')
    for table_name in ordered_table_names:
        with open(r'{}/{}.sql'.format(CREATE_TABLES_PATH,
                                      table_name)) as query_file:
            print('--Creating table {}'.format(table_name))
            query = query_file.read()
            DBconn.run_query(query, with_commit=True)
