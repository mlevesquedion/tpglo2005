SELECT id_source AS id
FROM demandeAmitie
WHERE etat = TRUE
  AND %s = id_cible
UNION
SELECT id_cible AS id
FROM demandeAmitie
WHERE etat = TRUE
  AND %s = id_source
