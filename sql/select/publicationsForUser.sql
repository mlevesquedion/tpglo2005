SELECT
  pC.*,
  (SELECT TRUE FROM publicationAime WHERE id_publication=pC.id_publication AND id_utilisateur=%s) AS
    statut_aime
FROM publicationComplet AS pC
WHERE id_auteur=%s
  OR niveau_acces=TRUE
  OR id_auteur IN (
      SELECT id_source
      FROM demandeAmitie
      WHERE id_cible=%s AND etat=TRUE
      UNION
      SELECT id_cible
      FROM demandeAmitie
      WHERE id_source=%s AND etat=TRUE
  )
ORDER BY instant_publication DESC