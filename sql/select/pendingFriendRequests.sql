SELECT
  u.id AS id,
  u.nom AS nom,
  u.id_photo AS id_photo
FROM demandeAmitie dA
JOIN utilisateur u ON dA.id_source = u.id
WHERE dA.id_cible = %s AND etat=FALSE
