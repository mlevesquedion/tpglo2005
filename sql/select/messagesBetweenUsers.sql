SELECT id_source, id_cible, contenu, instant
FROM message
WHERE (id_source = %s AND id_cible = %s) OR (id_cible = %s AND id_source = %s)
ORDER BY instant