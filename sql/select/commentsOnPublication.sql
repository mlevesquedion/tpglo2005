SELECT
  pC.instant AS instant,
  pC.contenu AS contenu,
  u.id_photo AS id_photo,
  u.id AS id_auteur,
  u.nom AS nom_auteur
FROM publicationCommentaire pC
  JOIN utilisateur u ON pC.id_utilisateur = u.id
WHERE pC.id_publication = %s
ORDER BY instant ASC