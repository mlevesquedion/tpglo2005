import os

ordered_table_names = [
    'utilisateur',
    'message',
    'demandeAmitie',
    'amitie',
    'hashtag',
    'publication',
    'publicationHashtag',
    'publicationAime',
    'publicationCommentaire',
]


def read_query_file(query_type, filename):
    """Load contents of a sql file"""
    os.chdir(os.path.dirname(os.path.abspath(__file__)))
    filepath = r'{}\{}.sql'.format(query_type, filename)
    with open(filepath) as sql_file:
        return sql_file.read()