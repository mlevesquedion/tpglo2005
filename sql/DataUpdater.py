from sql.DataManager import DataManager
from sql.db_utils import read_query_file


class DataUpdater(DataManager):
    """High-level interface to the database to simplify and normalize
    the use of common UPDATE and DELETE queries"""

    def get_formatted_update_list(self, dictionary):
        """Creates a list of key=value pairs separated by commas,
        wrapping values in quotes if they are of type str"""
        return ','.join([
            str(k)
            + '='
            + ("'{}'".format(v) if isinstance(v, str) else str(v))
            for k, v in dictionary.items()
        ])

    def get_formatted_update_query(self, dictionary, filename, table=None):
        """Returns a properly formatted update query, given a table name,
        a tuple_id, a dictionary and a SQL file's name"""
        query = read_query_file('update', filename)
        equals = self.get_formatted_update_list(dictionary)
        formats = ([table] if table is not None else []) + [equals]
        formatted_query = query.format(*formats)
        return formatted_query

    def update_generic(self, tuple_id, dictionary, table, with_commit=True):
        """Updates a table's data with a dict, matching a given id"""
        query = self.get_formatted_update_query(dictionary, 'genericById', table)
        params = (tuple_id,)
        self.run_parameterized_query(query, params, with_commit)

    def update_user(self, tuple_id, dictionary):
        """Update a user's data with a dict, given an id"""
        self.update_generic(tuple_id, dictionary, 'utilisateur')

    def accept_friend_request(self, requester_id, acceptor_id):
        """Update a friend request's status to TRUE, meaning it has been accepted.
        'requester_id' and 'acceptor_id' correspond to 'id_source' and 'id_cible',
        respectively."""
        query = read_query_file('update', 'friendRequest')
        params = (requester_id, acceptor_id)
        self.run_parameterized_query(query, params)

    def change_publication_access_level(self, publication_id, new_access_level):
        """Change a publication's access level to private (FALSE) or public (TRUE)"""
        query = read_query_file('update', 'publicationAccessLevel')
        params = (new_access_level, publication_id)
        self.run_parameterized_query(query, params)

    def unfriend(self, user_id, friend_id):
        """Delete the friend request between two users, which deletes their friendship"""
        query = read_query_file('update', 'unfriend')
        params = (user_id, friend_id, user_id, friend_id)
        self.run_parameterized_query(query, params)

    def unlike(self, user_id, publication_id):
        """Delete a user's like on a publication"""
        query = read_query_file('update', 'unlike')
        params = (user_id, publication_id)
        self.run_parameterized_query(query, params)

    def delete_account_by_id(self, user_id):
        """Delete a user's account using id"""
        query = read_query_file('update', 'deleteAccountById')
        params = (user_id,)
        self.run_parameterized_query(query, params)

    def delete_account_by_email(self, user_email):
        """Delete a user's account using email instead of id"""
        query = read_query_file('update', 'deleteAccountByEmail')
        params = (user_email,)
        self.run_parameterized_query(query, params)

    def delete_comment(self, id_utilisateur, id_publication, timestamp):
        """Delete a comment made by a user on a given post around a given timestamp"""
        query = read_query_file('update', 'deleteComment')
        params = (id_utilisateur, id_publication, timestamp, timestamp)
        self.run_parameterized_query(query, params)

    def delete_publication(self, id_publication):
        """Delete a publication made by a user given its id"""
        query = read_query_file('update', 'deletePublication')
        params = (id_publication,)
        self.run_parameterized_query(query, params)


if __name__ == '__main__':
    data_updater = DataUpdater()
    print(data_updater.get_formatted_update_query('utilisateur', {'nom': 'Admin', 'sexe': 'male'}, 'genericById'))
