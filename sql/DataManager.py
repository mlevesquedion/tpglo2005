from sql.DatabaseConnection import DatabaseConnection


class DataManager:
    """Base class for classes that have to interface with the database"""

    def __init__(self, db_name='catnet'):
        """Initialize a DatabaseConnection, with a dict-type cursor. The results
        returned by this cursor can be indexed by field name"""
        self.db_conn = DatabaseConnection(db=db_name, cursor_type='dict')

    def run_parameterized_query(self, query, params=None, with_commit=True):
        """Returns results of a query, formatted with the given params (parameters)"""
        return self.db_conn.run_query(query, params, with_commit)

    def close(self):
        self.db_conn.close()