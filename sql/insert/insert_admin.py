def insert_admin(db_conn):
    """Insert a "default" user into the database.
    Mostly for debugging and testing purposes."""

    query = """\
    INSERT INTO utilisateur
    (email, mot_passe, nom, telephone, sexe, race, date_naissance, instant_inscription, id_photo)
    VALUES
    ('admin@gmail.com',
    '8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918',
    'Admin',
    '418 123-4567',
    'male',
    'Domestic',
    CURDATE() - INTERVAL 1 YEAR,
    CURRENT_TIMESTAMP() - INTERVAL 1 YEAR,
    'a01')
    """
    db_conn.run_query(query, with_commit=True)