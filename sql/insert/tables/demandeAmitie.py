import random
from data_mocking.mockers import *
from sql.insert.MockInserter import MockInserter


def insert_friend_requests(db_conn):

    friend_requests = []

    user_ids_query = "SELECT id FROM utilisateur"
    user_ids = {x[0] for x in db_conn.run_query(user_ids_query)}

    cutoff = 0.80

    for id_source in user_ids:
        for id_cible in (user_ids - {id_source}):

            if random.random() > cutoff:
                instant_inscription_query = """\
                SELECT MAX(instant_inscription)
                FROM utilisateur
                WHERE id IN ({}, {})""".format(id_source, id_cible)

                instant_inscription_max = db_conn.run_query(instant_inscription_query)[0][0]

                instant = get_timestamp(after=instant_inscription_max)

                etat = False

                friend_request = [id_source,
                        id_cible,
                        instant,
                        etat
                        ]

                friend_requests.append(friend_request)

    table_name = 'demandeAmitie'
    table_fields = 'id_source id_cible instant etat'.split()

    MockInserter.insert_values(
        db_conn,
        table_name,
        table_fields,
        friend_requests
    )
