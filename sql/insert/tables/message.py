from data_mocking.mockers import *
from sql.insert.MockInserter import MockInserter


def insert_messages(db_conn):

    table_name = 'message'
    table_fields = 'id_source id_cible instant contenu'.split()

    friendships_query = """\
           SELECT id_source, id_cible, instant_debut
           FROM amitieComplet"""
    friendships = db_conn.run_query(friendships_query)

    messages = []
    for id_source, id_cible, instant_debut in friendships:

        message_count = int(random.gammavariate(1, 2))

        for _ in range(message_count):
            instant = get_timestamp(after=instant_debut)
            contenu = get_content(max_lines=3)
            messages.append([id_source, id_cible, instant, contenu])

        message_count = int(random.gammavariate(1, 2))

        for _ in range(message_count):
            instant = get_timestamp(after=instant_debut)
            contenu = get_content(max_lines=3)
            messages.append([id_cible, id_source, instant, contenu])

    MockInserter.insert_values(
        db_conn,
        table_name,
        table_fields,
        messages
    )