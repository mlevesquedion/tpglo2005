from data_mocking.mockers import *
from sql.insert.MockInserter import MockInserter


def insert_publications(db_conn):

    table_name = 'publication'
    table_fields = 'id_utilisateur instant contenu niveau_acces'.split()

    user_ids_and_join_dates_query = """\
    SELECT id, instant_inscription
    FROM utilisateur"""

    user_ids_and_join_times = db_conn.run_query(user_ids_and_join_dates_query)

    publications = []
    for user_id, join_time in user_ids_and_join_times:
        n_publications = random.randint(0, 10)
        for _ in range(n_publications):
            instant = get_timestamp(after=join_time)
            contenu = get_content()
            niveau_acces = get_bool()
            publications.append([user_id, instant, contenu, niveau_acces])

    MockInserter.insert_values(
        db_conn,
        table_name,
        table_fields,
        publications
    )