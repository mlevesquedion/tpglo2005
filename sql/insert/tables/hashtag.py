from data_mocking.mockers import *
from sql.insert.MockInserter import MockInserter


def insert_hashtags(db_conn):

    table_name = 'hashtag'
    table_fields = ['nom']

    values = [[hashtag] for hashtag in get_all_hashtags()]

    MockInserter.insert_values(
        db_conn,
        table_name,
        table_fields,
        values
    )