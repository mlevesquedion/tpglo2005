from data_mocking.mockers import *
from sql.insert.MockInserter import MockInserter


def insert_publicationHashtags(db_conn):

    table_name = 'publicationHashtag'
    table_fields = 'id_publication nom_hashtag'.split()

    publication_ids_query = "SELECT id FROM publication"
    publication_ids = [t[0] for t in db_conn.run_query(publication_ids_query)]

    hashtag_names_query = "SELECT nom FROM hashtag"
    hashtag_names = [t[0] for t in db_conn.run_query(hashtag_names_query)]

    publicationHashtags = []
    for id_publication in publication_ids:
        hashtag_count = min(len(hashtag_names), int(random.gammavariate(3, 2)))
        publication_hashtag_names = random.sample(hashtag_names, hashtag_count)
        for publication_hashtag_name in publication_hashtag_names:
            publicationHashtags.append([id_publication, publication_hashtag_name])

    MockInserter.insert_values(
        db_conn,
        table_name,
        table_fields,
        publicationHashtags
    )