from data_mocking.mockers import *
from sql.insert.MockInserter import MockInserter


def insert_comments(db_conn):

    table_name = 'publicationCommentaire'
    table_fields = 'id_publication id_utilisateur instant contenu'.split()

    publications_query = "SELECT id, id_utilisateur, instant, niveau_acces FROM publication"
    publications = db_conn.run_query(publications_query)

    comments = []
    for id_publication, id_auteur, instant_publication, niveau_acces in publications:

        if niveau_acces:
            user_ids_query = "SELECT id FROM utilisateur"
        else:
            user_ids_query = """SELECT id
                             FROM utilisateur
                             WHERE id IN (
                               SELECT id_cible
                               FROM demandeAmitie
                               WHERE id_source = {} AND etat = TRUE
                             )""".format(id_auteur)

        user_ids = db_conn.run_query(user_ids_query)

        commenter_count = min(len(user_ids), int(random.gammavariate(2, 2)))

        commenter_ids = user_ids[:commenter_count]

        for id_utilisateur in commenter_ids:
            instant_commentaire = get_timestamp(after=instant_publication)
            contenu = get_content()
            comments.append([id_publication, id_utilisateur[0], instant_commentaire, contenu])

    MockInserter.insert_values(
        db_conn,
        table_name,
        table_fields,
        comments
    )