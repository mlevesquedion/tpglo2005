from data_mocking.mockers import *
from sql.insert.MockInserter import MockInserter


def insert_likes(db_conn):

    table_name = 'publicationAime'
    table_fields = 'id_publication id_utilisateur instant'.split()

    publications_query = "SELECT id, id_utilisateur, instant, niveau_acces FROM publication"
    publications = db_conn.run_query(publications_query)

    likes = []
    for id_publication, id_auteur, instant_publication, niveau_acces in publications:

        if niveau_acces:
            user_ids_query = "SELECT id FROM utilisateur"
        else:
            user_ids_query = """SELECT id
                             FROM utilisateur
                             WHERE id IN (
                               SELECT id_cible
                               FROM demandeAmitie
                               WHERE id_source = {} AND etat = TRUE
                             )""".format(id_auteur)

        user_ids = db_conn.run_query(user_ids_query)

        liker_count = min(len(user_ids), int(random.gammavariate(3, 2)))

        liker_ids = user_ids[:liker_count]

        for id_utilisateur in liker_ids:
            instant_aime = get_timestamp(after=instant_publication)
            likes.append([id_publication, id_utilisateur[0], instant_aime])

    MockInserter.insert_values(
        db_conn,
        table_name,
        table_fields,
        likes
    )