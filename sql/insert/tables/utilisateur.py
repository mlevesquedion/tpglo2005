from data_mocking.mockers import *
from sql.insert.MockInserter import MockInserter


def generate_user():
    sexe = get_sex()
    nom = get_name(sexe)
    email = get_email(nom)

    race = get_breed()
    mot_passe = get_hashed_password()
    telephone = get_telephone()
    id_photo = get_id_photo()

    date_naissance_object = get_date_object()
    date_naissance = str(date_naissance_object)
    instant_inscription = get_timestamp(after=date_naissance_object)

    return [email,
             mot_passe,
             nom,
             telephone,
             sexe,
             race,
             date_naissance,
             instant_inscription,
             id_photo
             ]


def insert_users(db_conn, n=100):

    table_name = 'utilisateur'
    table_fields = 'email mot_passe nom telephone sexe race date_naissance instant_inscription id_photo'.split()

    MockInserter.insert_tuples(db_conn, table_name, table_fields, generate_user, n)
