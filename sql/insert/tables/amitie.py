from pymysql.err import IntegrityError

from data_mocking.mockers import *


def insert_friendships(db_conn):

    user_ids_query = "SELECT id FROM utilisateur"
    user_ids = {x[0] for x in db_conn.run_query(user_ids_query)}

    cutoff = 0.05

    for user_id in user_ids:

        user_friend_requests_query = "SELECT id, instant FROM demandeAmitie WHERE id_source={}".format(user_id)
        user_friend_requests = {x for x in db_conn.run_query(user_friend_requests_query)}

        for request_id, request_timestamp in user_friend_requests:
            if random.random() > cutoff:
                instant_debut = get_timestamp(after=request_timestamp)
                insert_friendship_query = "INSERT INTO amitie (id_demande, instant_debut) VALUES ({}, '{}')".format(request_id, instant_debut)
                update_request_query = "UPDATE demandeAmitie SET etat = TRUE WHERE id = {}".format(request_id)
                db_conn.run_query(insert_friendship_query)
                db_conn.run_query(update_request_query)
