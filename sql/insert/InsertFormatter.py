class UnevenNumberOfFields(Exception):
    pass


class InsertFormatter:
    """This class provides facilities for formatting INSERT statements."""

    query_template = """\
    INSERT INTO {table_name}
    ({table_fields})
    VALUES
    {values}"""

    @staticmethod
    def get_partially_formatted_insert(table_name, table_fields, values):
        """Construct a valid SQL INSERT query from the given arguments,
        but does not fill in the values (%s is inserted instead)"""

        if not len(table_fields) == len(values):
            raise UnevenNumberOfFields

        params_string = '(' + ','.join('%s' for _ in table_fields) + ')'

        format_fields = {
            'table_name': table_name,
            'table_fields': ','.join(table_fields),
            'values': params_string
        }

        return InsertFormatter.query_template.format(**format_fields)

    @staticmethod
    def get_formatted_insert(table_name, table_fields, values):
        """Construct a valid SQL INSERT query from the given arguments"""

        if not InsertFormatter.even_lengths([table_fields] + [values[0]]):
            raise UnevenNumberOfFields

        values_string = InsertFormatter.get_values_string(values)

        format_fields = {
            'table_name': table_name,
            'table_fields': ','.join(table_fields),
            'values': values_string
        }

        return InsertFormatter.query_template.format(**format_fields)

    @staticmethod
    def get_values_string(values):
        """Convert a list of values to valid SQL for use in an INSERT query"""

        if not InsertFormatter.even_lengths(values):
            raise UnevenNumberOfFields

        value_strings = []

        for value in values:
            value_string = '('
            formatted_fields = ["'{}'".format(field) if isinstance(field, str)
                                else str(field) for field in value]
            value_string += ','.join(formatted_fields)
            value_string += ')'
            value_strings.append(value_string)

        values_string = ','.join(value_strings)

        return values_string

    @staticmethod
    def even_lengths(lst):
        """Check if the lists passed in are all of the same length"""
        return len(set(len(x) for x in lst)) == 1
