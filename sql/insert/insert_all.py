"""
Insert data into every table.
"""

from sql.insert.tables.utilisateur import insert_users
from sql.insert.tables.demandeAmitie import insert_friend_requests
from sql.insert.tables.amitie import insert_friendships
from sql.insert.tables.hashtag import insert_hashtags
from sql.insert.tables.publication import insert_publications
from sql.insert.tables.publicationAime import insert_likes
from sql.insert.tables.publicationCommentaire import insert_comments
from sql.insert.tables.publicationHashtag import insert_publicationHashtags
from sql.insert.tables.message import insert_messages


def insert_all(db_conn, n_users_to_insert=100):
    """Insert tuples into all tables. You may specify the number of users.
    Everything else (e.g., messages, posts) is handled by each of the insertion
    methods, based on the number of users."""

    print('\nINSERTING DATA INTO ALL TABLES')
    print('\n!!!THIS PROCESS MAY TAKE A FEW MINUTES!!!')

    print('\nINSERTING DATA INTO utilisateur')
    insert_users(db_conn, n_users_to_insert)

    print('\nINSERTING DATA INTO demandeAmitie')
    insert_friend_requests(db_conn)

    print('\nINSERTING DATA INTO amitie')
    insert_friendships(db_conn)

    print('\nINSERTING DATA INTO hashtag')
    insert_hashtags(db_conn)

    print('\nINSERTING DATA INTO publication')
    insert_publications(db_conn)

    print('\nINSERTING DATA INTO publicationAime')
    insert_likes(db_conn)

    print('\nINSERTING DATA INTO publicationCommentaire')
    insert_comments(db_conn)

    print('\nINSERTING DATA INTO publicationHashtags')
    insert_publicationHashtags(db_conn)

    print('\nINSERTING DATA INTO message')
    insert_messages(db_conn)
