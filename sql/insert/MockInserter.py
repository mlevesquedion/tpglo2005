from sql.insert.InsertFormatter import InsertFormatter
from pymysql.err import IntegrityError, InternalError


class MockInserter:
    """This class provides facilities for inserting tuples."""

    @staticmethod
    def insert_single_value(db_conn, table_name, table_fields, value):
        insert_query = InsertFormatter.get_formatted_insert(table_name,
                                                            table_fields,
                                                            [value])
        db_conn.run_query(insert_query, with_commit=True)

    @staticmethod
    def insert_values(db_conn, table_name, table_fields, values):
        """Insert a list of values, all at once (for when you know all values
        are valid)"""
        for value in values:
            try:
                MockInserter.insert_single_value(db_conn, table_name, table_fields, value)
            except (IntegrityError, InternalError) as e:
                pass

    @staticmethod
    def insert_tuples(db_conn, table_name, table_fields, generator_function, n_to_insert):
        """Insert a given number of tuples one at a time using a generator
        function (for when insertion may fail because of constraints)"""
        n_inserted = 0

        while n_inserted < n_to_insert:
            value = generator_function()
            try:
                MockInserter.insert_single_value(db_conn, table_name, table_fields, value)
            except (IntegrityError, InternalError) as e:
                pass
            else:
                n_inserted += 1
