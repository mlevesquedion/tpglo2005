from sql.DataManager import DataManager
from sql.db_utils import read_query_file


class DataFetcher(DataManager):
    """High-level interface to the database to simplify and normalize
    the use of common SELECT queries"""


    @staticmethod
    def get_formatted_select(match_field, field_names, table_name='utilisateur'):
        generic_select = read_query_file('select', 'generic')
        return generic_select.format(
            **{
                'field_names': ''.join(field_names),
                'table_name': table_name,
                'match_field': match_field
            }
        )

    def get_values_where_equals(self, field_names, compare_field, compare_value):
        query = self.get_formatted_select(compare_field, field_names)
        params = (compare_value,)
        results = self.run_parameterized_query(query, params)
        return results

    def get_password_by_email(self, user_email):
        """Returns a user's password, given his email"""
        result = self.get_values_where_equals(['mot_passe'], 'email', user_email)
        return result[0]['mot_passe'] if result else None

    def get_user_data_by_id(self, user_id):
        """Returns all data for a user, given the user's email"""
        result = self.get_values_where_equals(['*'], 'id', user_id)
        return result[0] if result else None

    def get_user_data_by_email(self, user_email):
        """Returns all data for a user, given the user's email"""
        result = self.get_values_where_equals(['*'], 'email', user_email)
        return result[0] if result else None

    def get_user_friend_ids(self, user_id):
        """Returns a list of a user's friends' ids"""
        query = read_query_file('select', 'userFriends')
        params = (user_id, user_id)
        results = self.run_parameterized_query(query, params)
        return [result['id'] for result in results]

    def get_user_friend_count(self, user_id):
        """Returns a user's friend count"""
        query = read_query_file('select', 'userFriendCount')
        params = (user_id,)
        results = self.run_parameterized_query(query, params)
        return results[0]['count']

    def get_users_data_by_name(self, user_name):
        """Returns all data for all users matching a given name"""
        return self.get_values_where_equals(['*'], 'nom', user_name)

    def get_publications_by_user(self, user_id):
        """Returns a list of all publications written by the user"""
        query = read_query_file('select', 'publicationsByUser')
        params = (user_id,)
        results = self.run_parameterized_query(query, params)
        return results

    def get_publications_for_user(self, user_id):
        """Returns a list of all publications the user can see (either public
        or published by friends)"""
        query = read_query_file('select', 'publicationsForUser')
        params = (user_id, user_id, user_id, user_id)
        results = self.run_parameterized_query(query, params)
        for publication in results:
            publication['commentaires'] = self.get_comments_for_publication(publication['id_publication'])
        return results

    def get_publications_for_user_by_hashtag(self, user_id, hashtag):
        query = read_query_file('select', 'publicationsForUserByHashtag')
        params = (user_id, user_id, user_id, user_id, hashtag)
        results = self.run_parameterized_query(query, params)
        for publication in results:
            publication['commentaires'] = self.get_comments_for_publication(publication['id_publication'])
        return results

    def get_comments_for_publication(self, publication_id):
        query = read_query_file('select', 'commentsOnPublication')
        params = (publication_id,)
        results = self.run_parameterized_query(query, params)
        return results

    def get_messages_between_users(self, id_1, id_2):
        """Returns a list of all messages exchanged by two users, given their ids"""
        query = read_query_file('select', 'messagesBetweenUsers')
        params = (id_1, id_2, id_1, id_2)
        results = self.run_parameterized_query(query, params)
        return results

    def get_pending_friend_requests(self, user_id):
        """Returns a list of all pending requests for a user"""
        query = read_query_file('select', 'pendingFriendRequests')
        params = (user_id,)
        results = self.run_parameterized_query(query, params)
        return results


if __name__ == '__main__':
    data_fetcher = DataFetcher()
    print(data_fetcher.get_user_data_by_email('admin@gmail.com'))
    print(data_fetcher.get_user_friend_count(1))
    print(list(data_fetcher.get_user_friend_ids(1)))
    print(list(data_fetcher.get_publications_by_user(1)))
    print(list(data_fetcher.get_publications_for_user(1)))
    print(list(data_fetcher.get_messages_between_users(1, 2)))
