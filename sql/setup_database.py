"""
This script resets the tables in the database, i.e. all tables are dropped,
re-created, and re-populated with new mock data.
"""

import os
from sql.reset_database import reset_database
from sql.insert.insert_all import insert_all
from sql.insert.insert_admin import insert_admin
from sql.create.create_all import create_all
from sql.create.create_triggers import create_utility_triggers
from sql.DatabaseConnection import DatabaseConnection

os.chdir(os.path.dirname(os.path.abspath(__file__)))


def setup_database(db_name='catnet', n_users_to_insert=100, with_admin=True, ask_before_reset=True):
    """Reset the database, create all tables, insert the admin account, then insert data into all tables"""
    reset_database(ask_before_reset, db_name)

    db_conn = DatabaseConnection(db=db_name)
    create_all(db_conn)

    if with_admin:
        insert_admin(db_conn)

    insert_all(db_conn, n_users_to_insert)
    create_utility_triggers(db_conn)

    print('\nDONE')

    return db_conn


if __name__ == '__main__':
    setup_database()
