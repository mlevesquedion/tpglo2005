"""
This script resets the tables in the database, i.e. all tables are dropped,
re-created, and re-populated with new mock data.
"""

from pymysql import InternalError
from sql.DatabaseConnection import DatabaseConnection


def drop_database(db_conn, db_name):
    try:
        print('\nDROPPING DATABASE {}'.format(db_name))
        db_conn.run_query('DROP DATABASE {}'.format(db_name), with_commit=True)
    except InternalError:
        print('Database {} does not exist!'.format(db_name))


def create_database(db_conn, db_name):
    print('\nCREATING DATABASE {}'.format(db_name))
    db_conn.run_query('CREATE DATABASE {}'.format(db_name), with_commit=True)


def reset_database(ask_before_reset=True, db_name='catnet'):

    if ask_before_reset:

        permission = None
        while permission not in ('y', 'n'):
            permission = input('You are about to reset the database. Proceed? (y/n) ')

        if permission == 'y':
            print('Database reset will proceed.')
        else:
            print('Database reset aborted.')
            exit(0)

    db_conn = DatabaseConnection(db=None)
    drop_database(db_conn, db_name)
    create_database(db_conn, db_name)


if __name__ == '__main__':
    reset_database()
