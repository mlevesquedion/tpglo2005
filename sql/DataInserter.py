from sql.insert.InsertFormatter import InsertFormatter
from sql.DataManager import DataManager


class DataInserter(DataManager):
    """High-level interface to the database to simplify and normalize
    the use of common INSERT queries"""
    pass


table_to_fields = {
    'demandeAmitie': 'id_source id_cible'.split(),
    'hashtag': 'nom'.split(),
    'message': 'id_source id_cible contenu'.split(),
    'publication': 'id_utilisateur contenu niveau_acces'.split(),
    'publicationHashtag': 'id_publication nom_hashtag'.split(),
    'publicationAime': 'id_publication id_utilisateur'.split(),
    'publicationCommentaire': 'id_publication id_utilisateur contenu'.split(),
    'utilisateur': 'email mot_passe nom telephone sexe race date_naissance id_photo'.split()
}


# A bit of metaprogramming to reduce the boilerplate
def add_insert_method(cls, table_name, table_fields):
    def insert_method(self, values):
        formatted_query = InsertFormatter.get_partially_formatted_insert(table_name, table_fields, values)
        self.run_parameterized_query(formatted_query, values)
    method_name = 'insert_{}'.format(table_name)
    insert_method.__name__ = method_name
    setattr(cls, method_name, insert_method)


for table_name, table_fields in table_to_fields.items():
    add_insert_method(DataInserter, table_name, table_fields)



if __name__ == '__main__':
    print('NOT IMPLEMENTED')
