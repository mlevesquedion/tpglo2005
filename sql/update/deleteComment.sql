DELETE FROM publicationCommentaire
WHERE
  id_utilisateur=%s
  AND id_publication=%s
  AND instant BETWEEN %s - INTERVAL 1 SECOND AND %s + INTERVAL 1 SECOND