import os
from getpass import getpass
import pymysql
from pymysql.cursors import DictCursor

# Hack en attendant de trouver une meilleure solution
abspath = os.path.abspath(__file__)
dname = os.path.dirname(abspath)
os.chdir(dname)

# TODO : Add prompt for password (vers la fin, sinon c'est dans les jambes)
with open(r'..\db_password.txt') as file:
    DEFAULT_PASSWORD = file.read()


class DatabaseConnection:
    """Wrapper around pymysql's connection and cursor to facilitate
    running queries and whatnot."""

    def __init__(self, host='localhost', port=3306, user='root', password=DEFAULT_PASSWORD, db='catnet', cursor_type='normal'):
        """Create a connection and cursor"""

        self.conn = pymysql.connect(
            host=host,
            port=port,
            user=user,
            password=password,
            db=db
        )
        self.cur = self.conn.cursor(cursor=(DictCursor if cursor_type == 'dict' else None))

    def close(self):
        self.conn.close()

    def run_query(self, query, params=None, with_commit=False):
        """Get results of a query. The "with_commit" argument must be used
        with any query that intends to modify the database (e.g., INSERT,
        UPDATE, DELETE, DROP...)"""
        self.cur.execute(query, params if params is not None else [])
        if with_commit:
            self.conn.commit()
        return self.cur.fetchall()

    def get_table_names(self):
        """Get a list of the names of the database's tables"""
        query = 'SHOW TABLES'
        table_names_tuples = self.run_query(query)
        table_names = [t[0] for t in table_names_tuples]
        return table_names

    def get_column_names(self, table_name):
        """Get a list of the names of the columns of a table"""
        query = 'DESCRIBE {}'.format(table_name)
        table_tuples = self.run_query(query)
        column_names = [t[0] for t in table_tuples]
        return column_names


if __name__ == '__main__':
    db_conn = DatabaseConnection()
    print('Table names: ', db_conn.get_table_names())
    print('Column names for table utilisateur:', db_conn.get_column_names('utilisateur'))
    print(db_conn.run_query('SELECT * FROM utilisateur WHERE id=%s', (1,)))