from flask import Flask, request, session, redirect, url_for, abort, \
    render_template, jsonify
from functools import wraps

from pymysql import IntegrityError

from sql.DatabaseConnection import DatabaseConnection
from sql.DataFetcher import DataFetcher
from sql.DataInserter import DataInserter
from sql.DataUpdater import DataUpdater
from data_mocking.mockers import get_id_photo
from hashlib import sha256
from datetime import datetime
from data_utils.data_utils import calculate_age
from emails.emails_utils import Emails
from data_mocking.mockers import get_password_combo

app = Flask(__name__)
app.config.from_object(__name__)

app.config.update(dict(
    SECRET_KEY='%7*%ifzMs4tK3nwR&gN8ln&aOY6IruMf'
))
app.config.from_envvar('FLASKR_SETTINGS', silent=True)

db = DatabaseConnection()
data_fetcher = DataFetcher()
data_inserter = DataInserter()
data_updater = DataUpdater()
email_sender = Emails()


def require_login(func):
    # Cette fonction es tirée de la documentation de flask
    @wraps(func)
    def wrapper(*args, **kwargs):
        if not session.get('logged_in'):
            return redirect(url_for('home'))
        return func(*args, **kwargs)
    return wrapper


@app.route('/')
def home():
    error = request.args.get('error', None)
    message = request.args.get('message', None)
    return render_template('home.html', error=error, message=message)


@app.route('/adduser', methods=['POST'])
def add_user():
    email, password, name, phone, sex, race, date_string = (request.form[field]
                                                            for field in
                                                            'email password ' \
                                                            'name telephone ' \
                                                            'sex race ' \
                                                            'date'.split())

    # Check that user doesn't already exist
    if data_fetcher.get_user_data_by_email(email):
        error = 'Vous avez déjà un compte. Veuillez vous connecter.'
        return redirect(url_for('home', error=error))

    # Add user to database
    password_bytes = bytes(password, encoding='utf-8')
    hashed_password = sha256(password_bytes).hexdigest()
    birth_date = datetime.strptime(date_string, '%Y-%m-%d').date()
    id_photo = get_id_photo()
    data_inserter.insert_utilisateur(
        [email, hashed_password, name, phone, sex, race, birth_date, id_photo])

    message = 'Votre compte a été créé avec succès. Vous pouvez maintenant ' \
              'vous connecter.'
    return redirect(url_for('home', message=message))


@app.route('/register', methods=['GET'])
def register():
    return render_template('register.html')


@app.route('/login', methods=['POST'])
def login():
    email, request_password = (request.form[field] for field in
                               ('email', 'password'))
    request_password_bytes = bytes(request_password, encoding='utf-8')
    request_hashed_password = sha256(request_password_bytes).hexdigest()

    db_hashed_password = data_fetcher.get_password_by_email(email)

    if request_hashed_password != db_hashed_password:
        error = "Connexion impossible. Nom d'utilisateur ou mot de passe " \
                "invalide."
        return redirect(url_for('home', error=error))

    else:
        session['logged_in'] = True
        session['email'] = email
        session['user'] = data_fetcher.get_user_data_by_email(email)

        return redirect(url_for('home'))


@app.route('/user_page/<int:user_id>/', methods=['GET'])
@require_login
def user_page(user_id):
    visited_user = data_fetcher.get_user_data_by_id(user_id)

    visited_user['age'] = calculate_age(visited_user)
    visited_user['friend_count'] = data_fetcher.get_user_friend_count(
        visited_user['id'])

    visiting_user = session['user']
    if visiting_user['id'] == visited_user['id']:
        closeness = 'user'
    elif visiting_user['id'] in data_fetcher.get_user_friend_ids(
            visited_user['id']):
        closeness = 'friend'
    else:
        closeness = 'stranger'

    visitedUserSentRequest = data_fetcher.run_parameterized_query('''\
    SELECT TRUE
    FROM demandeAmitie
    WHERE id_source={} AND id_cible={}'''.format(visited_user['id'],
                                                 visiting_user['id']))
    visitingUserSentRequest = data_fetcher.run_parameterized_query('''\
    SELECT TRUE
    FROM demandeAmitie
    WHERE id_source={} AND id_cible={}'''.format(visiting_user['id'],
                                                 visited_user['id']))

    return render_template('user_page.html',
                           user=visited_user,
                           visitor=visiting_user,
                           closeness=closeness,
                           visitedUserSentRequest=visitedUserSentRequest,
                           visitingUserSentRequest=visitingUserSentRequest)


@app.route('/publications/', methods=['GET'])
@require_login
def publications():
    user_id = session['user']['id']
    publications = data_fetcher.get_publications_for_user(user_id)[:10]
    return render_template('publications.html', publications=publications, user=session['user'])


@app.route('/hashtagsearch/<string:hashtag>', methods=['GET'])
@require_login
def publications_by_hashtag(hashtag):
    user_id = session['user']['id']
    publications = data_fetcher.get_publications_for_user_by_hashtag(user_id, hashtag)[:10]
    return render_template('publications.html', publications=publications, user=session['user'])


@app.route('/submitpost/', methods=['POST'])
@require_login
def submit_post():
    user_id = session['user']['id']
    contenu, hashtags = (request.form[field] for field in ('contenu', 'hashtags'))
    niveau_acces = request.form.get('niveau_acces', None) is None
    data_inserter.insert_publication([user_id, contenu, niveau_acces])
    last_publication_id = data_fetcher.run_parameterized_query('''\
    SELECT id
    FROM publication
    WHERE id_utilisateur=%s
    ORDER BY INSTANT DESC
    LIMIT 1''', user_id)[0]['id']
    if hashtags:
        for hashtag in hashtags.split(','):
            try:
                data_inserter.insert_hashtag([hashtag])
            except IntegrityError:
                # Hashtag already in database
                pass
            data_inserter.insert_publicationHashtag([last_publication_id, hashtag])
    return redirect(url_for('publications'))


@app.route('/submitcomment/', methods=['POST'])
@require_login
def submit_comment():
    user_id = session['user']['id']
    id_publication, contenu = (request.form[field] for field in ('id_publication', 'contenu'))
    data_inserter.insert_publicationCommentaire([id_publication, user_id, contenu])
    return '', 200


@app.route('/deletecomment/', methods=['POST'])
@require_login
def delete_comment():
    id_utilisateur = session['user']['id']
    id_publication, timestamp_string = (request.form[field] for field in ('id_publication', 'timestamp'))
    try:
        timestamp = datetime.strptime(timestamp_string, '%d-%m-%Y %H:%M:%S')
    except ValueError:
        timestamp = datetime.strptime(timestamp_string, '%Y-%m-%d %H:%M:%S')
    data_updater.delete_comment(id_utilisateur, id_publication, timestamp)
    return '', 200


@app.route('/deletepublication/', methods=['POST'])
@require_login
def delete_publication():
    id_publication = request.form['id_publication']
    data_updater.delete_publication(id_publication)
    return '', 200


@app.route('/toggle_like/', methods=['POST'])
@require_login
def toggle_like():
    publication_id = int(request.form['publication_id'])
    user_id = session['user']['id']

    action = request.form['action']

    if action == 'like':
        data_inserter.insert_publicationAime([publication_id, user_id])
    elif action == 'unlike':
        data_updater.unlike(user_id, publication_id)

    return '', 200


@app.route('/user_edit/<int:user_id>/', methods=['GET', 'POST'])
@require_login
def user_edit(user_id):
    user = session['user']
    error = None

    if request.method == 'GET':
        return render_template('user_edit.html', user=user, error=error)

    elif request.method == 'POST':
        email, phone = (request.form[field] for field in ('email', 'telephone'))

        data_updater.update_user(user_id, {
            'email': email,
            'telephone': phone
        })

        session['user'] = data_fetcher.get_user_data_by_email(email)
        user = session['user']

        message = "Informations mises à jour avec succès."
        return render_template('user_edit.html', user=user, message=message)


@app.route('/friend_requests/')
@require_login
def friend_requests():
    results = data_fetcher.get_pending_friend_requests(session['user']['id'])
    return render_template('friend_requests.html', users=results)


@app.route('/logout')
@require_login
def logout():
    session.pop('logged_in', None)
    session.pop('email', None)
    session.pop('user', None)
    message = 'Vous vous êtes déconnecté avec succès.'
    return render_template('home.html', message=message)


@app.route('/getmessagesfromsingleuser', methods=['GET'])
@require_login
def get_messages_from_single_user():
    current_user_id = (session['user']['id'])
    destinataire_id = request.form['destinataire']
    messages = db.run_query(
        'SELECT contenu, instant, etat FROM message WHERE id_source= %i AND '
        'id_cible= %i ORDER BY instant desc' % (
            current_user_id, destinataire_id,))
    return render_template('user_page.html', messages=messages)


@app.route('/messages', methods=['GET'])
@require_login
def messages():
    current_user_id = (session['user']['id'])
    friends_ids = data_fetcher.get_user_friend_ids(current_user_id)
    friendlist = {}
    for friend_id in friends_ids:
        friend_data = data_fetcher.get_user_data_by_id(friend_id)
        friend_data.pop("email")
        friend_data.pop("mot_passe")
        friend_data.pop("telephone")
        friend_data.pop("sexe")
        friend_data.pop("date_naissance")
        friend_data.pop("instant_inscription")
        friendlist[friend_data['id']] = friend_data
    return render_template('messages.html', user=session['user'], friendlist=friendlist)


@app.route('/messages/getfrom/<int:friend_id>', methods=['GET'])
@require_login
def get_all_messages(friend_id):
    current_user_id = (session['user']['id'])
    messages = data_fetcher.get_messages_between_users(current_user_id, friend_id)
    for message in messages:
        message['instant'] = str(message['instant'].replace(microsecond=0))
    return jsonify(messages), 200


@require_login
@app.route('/messages/send/<int:friend_id>', methods=['POST'])
def send_message(friend_id):
    current_user_id = (session['user']['id'])
    message = request.form['message']
    data_inserter.insert_message((current_user_id, friend_id, message))
    return '', 200


@app.route('/sendfriendrequest/', methods=['POST'])
@require_login
def send_friend_request():
    src_id, dest_id = (int(request.form[field]) for field in
                       ('src_id', 'dest_id'))
    data_inserter.insert_demandeAmitie([src_id, dest_id])
    return redirect(url_for('user_page', user_id=dest_id))


@app.route('/acceptfriendrequest/', methods=['POST'])
@require_login
def accept_friend_request():
    src_id, dest_id = (int(request.form[field]) for field in
                       ('src_id', 'dest_id'))
    data_updater.accept_friend_request(src_id, dest_id)
    return redirect(url_for('user_page', user_id=src_id))


@app.route('/unfriend/', methods=['POST'])
@require_login
def unfriend():
    src_id, dest_id = (int(request.form[field]) for field in
                       ('src_id', 'dest_id'))
    data_updater.unfriend(src_id, dest_id)
    return redirect(url_for('user_page', user_id=dest_id))


@app.route('/searchbyname', methods=['POST'])
@require_login
def search_user_by_name():
    search_name = request.form['search_txt']
    results = data_fetcher.get_users_data_by_name(search_name)
    return render_template('user_search.html', name=search_name, users=results)


@app.route('/lostpassword')
def lost_password():
    return render_template('lost_password.html')


@app.route('/lostpassword', methods=['POST'])
def send_lost_password_email():
    email = request.form['email']
    user_data = data_fetcher.get_user_data_by_email(email)
    if user_data == None:
        return render_template('lost_password_sent.html')
    new_password_and_hash = get_password_combo()
    data_updater.update_user(user_data['id'], {'mot_passe':new_password_and_hash[1]})
    messageText = "Votre nouveau mot de passe est : " + new_password_and_hash[0]
    message = email_sender.create_email(email, "CATNET - Nouveau mot de passe", messageText)
    email_sender.send_email(message)
    return render_template('lost_password_sent.html')


@app.route('/deleteaccount/', methods=['POST'])
@require_login
def delete_account():
    data_updater.delete_account_by_id(session['user']['id'])
    session.pop('logged_in', None)
    session.pop('email', None)
    session.pop('user', None)
    message = 'Votre compte a été supprimé avec succès.'
    return redirect(url_for('home', message=message))


for error_code in (400, 401, 404, 500):
    @app.errorhandler(error_code)
    def page_not_found(e, error_code=error_code):
        return render_template('{}.html'.format(error_code)), error_code


if __name__ == '__main__':
    app.run()
