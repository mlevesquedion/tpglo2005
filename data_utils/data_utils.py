from datetime import date


def calculate_age(user):
    user_birth_date = user['date_naissance']

    if isinstance(user_birth_date, str):
        pass

    today = date.today()
    user_age = today - user_birth_date
    return user_age.days // 365