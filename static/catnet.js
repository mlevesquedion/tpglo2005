var getMessages = function (friend)
{
    var messageContainer = document.getElementById('message-container');
    while (messageContainer.hasChildNodes()){
        messageContainer.removeChild(messageContainer.firstChild);
    }
    var xmlHttp = new XMLHttpRequest();
    xmlHttp.open("GET", window.location.href.replace('#','')+"/getfrom/"+friend['id'], true);
    xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    xmlHttp.send();
    xmlHttp.onreadystatechange = function () {
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200) {
            var messages = JSON.parse(xmlHttp.responseText);
            for (var key in messages)
            {
                var para = document.createElement("P");
                para.innerHTML = messages[key]['contenu'] + "<br><small class='messages-timestamp'>" + messages[key]['instant'] + "</small>";
                para.name = "message-" + friend['id'];
                para.classList.add(messages[key]['id_source'] == friend['id'] ? 'message-entrant' : 'message-sortant');
                messageContainer.appendChild(para);
            }
            displayMessageBox(friend);
        }
    }
};

var displayMessageBox = function (friend)
{
    var messageSenderContainer = document.getElementById('message-sender');
    while(messageSenderContainer.hasChildNodes()){
        messageSenderContainer.removeChild(messageSenderContainer.firstChild);
    }

    var messageBox = document.createElement("input")
    messageBox.type = "text";
    messageBox.id = "message-textBox";
    messageBox.name = friend['id'];
    messageBox.classList.add("message-textBox");
    messageBox.addEventListener("keypress", sendMessageWithEnter, false);
    messageBox.friend = friend;
    messageSenderContainer.appendChild(messageBox);
};

function sendMessageWithEnter(e){
    var keyCode = e.keyCode;
    var messageBox = document.getElementById("message-textBox");
    if(keyCode == 13){
        var xmlHttp = new XMLHttpRequest();
        xmlHttp.open("POST", window.location.href.replace('#','')+"/send/"+messageBox.friend['id'], true);
        xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlHttp.send("message="+messageBox.value);
        messageBox.value = "";
        getMessages(messageBox.friend)
    }
};

function addActiveId(e) {
    if (document.getElementById("active")){
        document.getElementById("active").removeAttribute("id");
    }
    e.target.setAttribute("id", "active");
    window.scrollTo(0, 0);
};

